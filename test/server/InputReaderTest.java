package server;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class InputReaderTest {
    private String requestLine = "GET /kareva.txt HTTP/1.1";
    private String header = "Host:           localhost:     7777";
    private String header2 = "Connection: keep-alive";
    private String header3 = "Cache-Control: max-age=0";
    private String header4 = "Upgrade-Insecure-Requests: 1";
    private String header5 = "Content-Length: 1";
    private List<String> inputListWithHeadersWithoutContentLength = Arrays.asList(requestLine, header, header2, header3, header4);
    private List<String> inputListWithAllHeaders = Arrays.asList(requestLine, header, header2, header3, header4, header5);
    private InputReader inputReader = new InputReader();

    @Test
    public void returnsIntGreaterThan0IfInputContainsContentLength() {
        assertEquals(0, inputReader.getContentLengthIfExists(inputListWithHeadersWithoutContentLength));
        assertTrue(inputReader.getContentLengthIfExists(inputListWithAllHeaders)>0);
    }
}