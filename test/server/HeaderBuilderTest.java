package server;

import org.junit.Test;

import static org.junit.Assert.*;

public class HeaderBuilderTest {
    private HeaderBuilder builder = new HeaderBuilder();
    private StringParser parser = new StringParser();

    @Test
    public void returnsCorrectContentLengthHeaderForNotFiles() {
        ServerUri uriEmptyUri = new ServerUri();
        assertEquals("Content-Length", builder.buildContentLength(uriEmptyUri).name);
        assertEquals("6", builder.buildContentLength(uriEmptyUri).value);

        ServerUri uriGoodbye = new ServerUri("goodbye");
        assertEquals("Content-Length", builder.buildContentLength(uriGoodbye).name);
        assertEquals("7", builder.buildContentLength(uriGoodbye).value);

        ServerUri uriHelloKairi = new ServerUri("kairi");
        assertEquals("Content-Length", builder.buildContentLength(uriHelloKairi).name);
        assertEquals("11", builder.buildContentLength(uriHelloKairi).value);

        ServerUri uriWithQuery = parser.getUriFromString("yo?name=foo");
        assertEquals("Content-Length", builder.buildContentLength(uriWithQuery).name);
        assertEquals("6", builder.buildContentLength(uriWithQuery).value);
    }

    @Test
    public void returnsCorrectContentTypeHeaderForNotFiles() {
        ServerUri uriEmptyUri = new ServerUri();
        assertEquals("Content-Type", builder.buildContentType(uriEmptyUri).name);
        assertEquals("text/plain; charset=utf-8", builder.buildContentType(uriEmptyUri).value);

        ServerUri uriGoodbye = new ServerUri("goodbye");
        assertEquals("Content-Type", builder.buildContentType(uriGoodbye).name);
        assertEquals("text/plain; charset=utf-8", builder.buildContentType(uriGoodbye).value);

        ServerUri uriHelloKairi = new ServerUri("kairi");
        assertEquals("Content-Type", builder.buildContentType(uriHelloKairi).name);
        assertEquals("text/plain; charset=utf-8", builder.buildContentType(uriHelloKairi).value);

        ServerUri uriWithQuery = parser.getUriFromString("yo?name=foo");
        assertEquals("Content-Type", builder.buildContentType(uriWithQuery).name);
        assertEquals("text/plain; charset=utf-8", builder.buildContentType(uriWithQuery).value);
    }

    @Test
    public void returnsCorrectContentLengthHeaderForFiles() {
        ServerUri uriKareva = new ServerUri("kareva.txt");
        assertEquals("Content-Length", builder.buildContentLength(uriKareva).name);
        assertEquals("389", builder.buildContentLength(uriKareva).value);
    }

    @Test
    public void returnsCorrectContentTypeHeaderForFiles() {
        ServerUri uriKareva = new ServerUri("kareva.txt");
        assertEquals("Content-Type", builder.buildContentType(uriKareva).name);
        assertEquals("text/plain; charset=utf-8", builder.buildContentType(uriKareva).value);

        ServerUri uriHtml = new ServerUri("testfile.html");
        assertEquals("Content-Type", builder.buildContentType(uriHtml).name);
        assertEquals("text/html; charset=utf-8", builder.buildContentType(uriHtml).value);

        ServerUri uriJpg = new ServerUri("testfile.jpg");
        assertEquals("Content-Type", builder.buildContentType(uriJpg).name);
        assertEquals("image/jpeg; charset=utf-8", builder.buildContentType(uriJpg).value);

        ServerUri uriPdf = new ServerUri("testfile.pdf");
        assertEquals("Content-Type", builder.buildContentType(uriPdf).name);
        assertEquals("application/pdf; charset=utf-8", builder.buildContentType(uriPdf).value);

        ServerUri uriIco = new ServerUri("favicon.ico");
        assertEquals("Content-Type", builder.buildContentType(uriIco).name);
        assertEquals("image/x-icon", builder.buildContentType(uriIco).value);
    }

    @Test
    public void buildsCorrectLocationHeader() {
        assertEquals("Location", builder.buildLocation().name);
        assertEquals("/", builder.buildLocation().value);
    }

    @Test
    public void buildsReadableStringFromHeader() {
        Header header = new Header("Content-Length", "11");
        assertEquals("Content-Length: 11\n", builder.getHeaderAsString(header));
    }
}