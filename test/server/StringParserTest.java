package server;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class StringParserTest {
    private StringParser parser = new StringParser();

    @Test
    public void returnsCorrectMethodFromString() {
        assertEquals(RequestMethod.GET, parser.getMethodFromString("GET"));
        assertEquals(RequestMethod.HEAD, parser.getMethodFromString("HEAD"));
    }

    @Test
    public void returnsCorrectHeaderFromString() {
        assertEquals("asdf", parser.getHeaderFromString("asdf: fdsa").name);
        assertEquals("fdsa", parser.getHeaderFromString("asdf: fdsa").value);
    }

    @Test
    public void returnsCorrectServerUri() {
        assertEquals("kareva.txt", parser.getUriFromString("kareva.txt").path);
        assertTrue(parser.getUriFromString("kareva.txt").queries.isEmpty());

        assertEquals("kareva", parser.getUriFromString("kareva?is=poet").path);
        assertEquals("is", parser.getUriFromString("kareva?is=poet").queries.get(0).key);
        assertEquals("poet", parser.getUriFromString("kareva?is=poet").queries.get(0).value);
        assertEquals(1, parser.getUriFromString("kareva?is=poet").queries.size());

        assertEquals("kareva", parser.getUriFromString("kareva?is=poet&age=noidea").path);
        assertEquals("is", parser.getUriFromString("kareva?is=poet&age=noidea").queries.get(0).key);
        assertEquals("poet", parser.getUriFromString("kareva?is=poet&age=noidea").queries.get(0).value);
        assertEquals("age", parser.getUriFromString("kareva?is=poet&age=noidea").queries.get(1).key);
        assertEquals("noidea", parser.getUriFromString("kareva?is=poet&age=noidea").queries.get(1).value);
        assertEquals(2, parser.getUriFromString("kareva?is=poet&age=noidea").queries.size());
    }
}