package server;

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class StatusLineBuilderTest {

    private RequestLine requestLine200 = new RequestLine(RequestMethod.GET, new ServerUri("/"), "HTTP");
    private Request request200 = new Request(requestLine200, new ArrayList<Header>());
    private RequestLine requestLine404 = new RequestLine(RequestMethod.GET, new ServerUri("/nosuchfile.txt"), "version");
    private Request request404 = new Request(requestLine404, new ArrayList<Header>());
    private StatusLineBuilder builder = new StatusLineBuilder();

    @Test
    public void returnsAdequateStatusLineParts() {
        assertEquals("200 OK", builder.builder(request200).StatusPhrase);
        assertEquals("HTTP", builder.builder(request200).httpVersion);

        assertEquals("404 Not Found", builder.builder(request404).StatusPhrase);
        assertEquals("version", builder.builder(request404).httpVersion);
    }
}