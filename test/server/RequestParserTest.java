package server;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class RequestParserTest {
    private String requestLine = "GET /kareva.txt HTTP/1.1";
    private String header = "Host:           localhost:     7777";
    private String header2 = "Connection: keep-alive";
    private String header3 = "Cache-Control: max-age=0";
    private String header4 = "Upgrade-Insecure-Requests: 1";
    private List<String> inputListWithHeaders = Arrays.asList(requestLine, header, header2, header3, header4);
    private RequestParser requestParser = new RequestParser();
    private StringParser stringParser = new StringParser();

    @Test
    public void detectsGETMethod() {
        assertEquals(RequestMethod.GET, stringParser.getMethodFromString("GET"));
    }

    @Test
    public void detectsGETMethodInFullRequest() {
        Request request = requestParser.parseInputLinesToRequest(inputListWithHeaders);

        assertEquals(RequestMethod.GET, request.requestLine.requestMethod);
        assertEquals(new ServerUri("kareva.txt").path, request.requestLine.uri.path);
        assertEquals("HTTP/1.1", request.requestLine.httpVersion);
        assertEquals("Host", request.headers.get(0).name);
        assertEquals("localhost:     7777", request.headers.get(0).value);
        assertEquals("Connection", request.headers.get(1).name);
        assertEquals("keep-alive", request.headers.get(1).value);
        assertEquals("Cache-Control", request.headers.get(2).name);
        assertEquals("max-age=0", request.headers.get(2).value);
        assertEquals("Upgrade-Insecure-Requests", request.headers.get(3).name);
        assertEquals("1", request.headers.get(3).value);
    }

    @Test
    public void doesNotProduceAnyExcessHeaders() {
        Request request = requestParser.parseInputLinesToRequest(inputListWithHeaders);
        assertEquals(4, request.headers.size());
    }

    @Test
    public void returnsHeaderFromString() {
        assertEquals("Papapa", stringParser.getHeaderFromString("Papapa: fafafa").name);
        assertEquals("fafafa", stringParser.getHeaderFromString("Papapa: fafafa").value);
    }

    @Test
    public void returnsCorrectMethodFromString() {
        assertEquals(RequestMethod.HEAD, stringParser.getMethodFromString("head"));
        assertEquals(RequestMethod.HEAD, stringParser.getMethodFromString("Head"));
        assertEquals(RequestMethod.HEAD, stringParser.getMethodFromString("HEAD"));
        assertEquals(RequestMethod.GET, stringParser.getMethodFromString("get"));
        assertEquals(RequestMethod.GET, stringParser.getMethodFromString("Get"));
        assertEquals(RequestMethod.GET, stringParser.getMethodFromString("GET"));
    }

    @Test
    public void returnsServerUriWithCorrectPathFromString() {
        ServerUri uri = stringParser.getUriFromString("/kareva.txt");
        ServerUri nonFileUri = stringParser.getUriFromString("/kareva");
        ServerUri uriWithHello = stringParser.getUriFromString("/hello?");
        ServerUri uriWithHelloName = stringParser.getUriFromString("/hello?name=nimi");
        ServerUri uriWithHelloNameAge = stringParser.getUriFromString("/hello?name=nimi&age=30");
        ServerUri emptyUri = stringParser.getUriFromString("/");
        assertEquals("/kareva.txt", uri.path);
        assertEquals("/kareva", nonFileUri.path);
        assertEquals("/hello", uriWithHello.path);
        assertEquals("/hello", uriWithHelloName.path);
        assertEquals("/hello", uriWithHelloNameAge.path);
        assertEquals("/", emptyUri.path);
    }

    @Test
    public void returnsServerUriWithCorrectQueriesFromString() {
        ServerUri uri = stringParser.getUriFromString("/kareva.txt");
        ServerUri nonFileUri = stringParser.getUriFromString("/kareva");
        ServerUri uriWithHello = stringParser.getUriFromString("/hello?");
        ServerUri uriWithHelloName = stringParser.getUriFromString("/hello?name=nimi");
        ServerUri uriWithHelloNameAge = stringParser.getUriFromString("/hello?name=nimi&age=30");
        ServerUri emptyUri = stringParser.getUriFromString("/");
        assertTrue(uri.queries.isEmpty());
        assertTrue(nonFileUri.queries.isEmpty());
        assertTrue(uriWithHello.queries.isEmpty());
        assertEquals("nimi", uriWithHelloName.queries.get(0).value);
        assertEquals("nimi", uriWithHelloNameAge.queries.get(0).value);
        assertEquals("30", uriWithHelloNameAge.queries.get(1).value);
        assertTrue(emptyUri.queries.isEmpty());

    }
}