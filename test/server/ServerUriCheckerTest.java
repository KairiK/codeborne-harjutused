package server;

import org.junit.Test;

import static org.junit.Assert.*;

public class ServerUriCheckerTest {
    private ServerUriChecker checker = new ServerUriChecker();

    @Test
    public void returnsTrueIfUriIsValid() {
        assertTrue(checker.uriIsValidAndCanBeResponded(new ServerUri("hello")));
        assertTrue(checker.uriIsValidAndCanBeResponded(new ServerUri("goodbye")));
        assertTrue(checker.uriIsValidAndCanBeResponded(new ServerUri("kareva.txt")));
        assertTrue(checker.uriIsValidAndCanBeResponded(new ServerUri("hello?name=nimi")));
        assertTrue(checker.uriIsValidAndCanBeResponded(new ServerUri("testfile.jpg")));

        assertFalse(checker.uriIsValidAndCanBeResponded(new ServerUri("testfile.jpeg")));
        assertFalse(checker.uriIsValidAndCanBeResponded(new ServerUri("nosuchfile.file")));
        assertFalse(checker.uriIsValidAndCanBeResponded(new ServerUri("aiki.trumm")));
    }

    @Test
    public void returnsTrueIfReferenceIsAFile() {
        assertTrue(checker.isReferenceAFile("kareva.txt"));
        //todo - this shouldn't probably be a file
        assertTrue(checker.isReferenceAFile("aiki.trumm"));
    }

    @Test
    public void returnsTrueIfFileExists() {
        assertTrue(checker.doesFileExist("kareva.txt"));
        assertFalse(checker.doesFileExist("nosuchfile.jpeg"));
    }
}