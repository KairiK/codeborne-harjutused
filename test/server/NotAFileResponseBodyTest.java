package server;

import org.junit.Test;

import java.util.Collections;

import static org.junit.Assert.assertEquals;

public class NotAFileResponseBodyTest {
    private NotAFileResponseBody responseBody = new NotAFileResponseBody();

    @Test
    public void returnsGoodbyeWhenEmpty() {
        assertEquals("Goodbye", responseBody.returnGoodbyeResponse());
    }

    @Test
    public void returnsHelloPlusName() {
        assertEquals("Hello Kairi", responseBody.returnHelloNameResponse("Kairi"));
    }

    @Test
    public void returnsCorrectResponseWhenUriHasQueryStrings() {
        ServerUri uri = new ServerUri("maja", Collections.singletonList(new ServerQuery("name", "house")));
        assertEquals("maja house", responseBody.returnPathWithQueryStringResponse(uri));
    }
}