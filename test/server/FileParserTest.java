package server;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class FileParserTest {

    @Test
    public void convertsFileToByteArray() {
        String expectedOutput = "<!DOCTYPE html>\n<html lang=\"en\">\n<head>\n    <meta charset=\"UTF-8\">\n    <title>Hello World!</title>\n</head>\n<body>\n<p>Hello world!</p>\n</body>\n</html>";
        assertArrayEquals(expectedOutput.getBytes(), FileParser.fileToByteArray("testfile.html"));
    }

    @Test
    public void detectsFileType() {
        assertEquals("txt", FileParser.findFileType("kareva.txt"));
        assertEquals("pdf", FileParser.findFileType("testfile.pdf"));
        assertEquals("jpeg", FileParser.findFileType("pilt.jpeg"));
        assertEquals("ico", FileParser.findFileType("favicon.ico"));
    }
}