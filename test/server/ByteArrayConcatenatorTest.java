package server;

import org.junit.Test;

import static org.junit.Assert.*;

public class ByteArrayConcatenatorTest {
    private ByteArrayConcatenator concat = new ByteArrayConcatenator();

    @Test
    public void concatenatesArrays() {
        byte[] a = new byte[]{1, 2};
        byte[] b = new byte[]{1, 2};
        assertArrayEquals(new byte[]{1, 2, 1, 2}, concat.concatenate(a, b));
    }
}