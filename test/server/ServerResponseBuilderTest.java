package server;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ServerResponseBuilderTest {

    @Test
    public void returnsAdequateResponseWithNoFile() {
        RequestLine line = new RequestLine(RequestMethod.GET, new ServerUri("sayhello"), "http");
        Header header = new Header("KnockKnock", "WhosThere");
        List<Header> headers = new ArrayList<>();
        headers.add(header);
        Request request = new Request(line, headers);

        ServerResponseBuilder builder = new ServerResponseBuilder();
        ServerResponse response = builder.buildFullResponse(request);

        assertEquals("200 OK", response.statusLine.StatusPhrase);
        assertEquals("http", response.statusLine.httpVersion);
        assertEquals("Content-Type", response.headers.get(0).name);
        assertEquals("text/plain; charset=utf-8", response.headers.get(0).value);
        assertEquals("Content-Length", response.headers.get(1).name);
        assertEquals("14", response.headers.get(1).value);
        assertEquals("Hello sayhello".getBytes().length, response.messageBody.length);
    }

    @Test
    public void returnsAdequateResponseFileWithHeadRequest() {
        RequestLine line = new RequestLine(RequestMethod.HEAD, new ServerUri("kareva.txt"), "http");
        Header header = new Header("KnockKnock", "WhosThere");
        List<Header> headers = new ArrayList<>();
        headers.add(header);
        Request request = new Request(line, headers);

        ServerResponseBuilder builder = new ServerResponseBuilder();
        ServerResponse response = builder.buildFullResponse(request);

        assertEquals("200 OK", response.statusLine.StatusPhrase);
        assertEquals("http", response.statusLine.httpVersion);
        assertEquals("Content-Type", response.headers.get(0).name);
        assertEquals("text/plain; charset=utf-8", response.headers.get(0).value);
        assertEquals("Content-Length", response.headers.get(1).name);
        assertEquals("389", response.headers.get(1).value);
        assertEquals(new byte[]{}.length,response.messageBody.length);
    }

    @Test
    public void returnsAdequate404ResponseWithFile() {
        RequestLine line = new RequestLine(RequestMethod.GET, new ServerUri("nosuchfile.txt"), "http");
        Header header = new Header("KnockKnock", "WhosThere");
        List<Header> headers = new ArrayList<>();
        headers.add(header);
        Request request = new Request(line, headers);

        ServerResponseBuilder builder = new ServerResponseBuilder();
        ServerResponse response = builder.buildFullResponse(request);

        assertEquals("404 Not Found", response.statusLine.StatusPhrase);
        assertEquals("http", response.statusLine.httpVersion);
        assertEquals("Content-Type", response.headers.get(0).name);
        assertEquals("image/png; charset=utf-8", response.headers.get(0).value);
        assertEquals("Content-Length", response.headers.get(1).name);
        assertEquals("27318", response.headers.get(1).value);
    }
}