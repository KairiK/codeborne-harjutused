package projectsBeforeServer;

import org.junit.Test;
import projectsBeforeServer.Concat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class ConcatTest {
    private Concat concat = new Concat();

    @Test
    public void concatenatesTwoLists() {
        List<String> firstWords = new ArrayList<>(Arrays.asList("tere", "tõre"));
        List<String> secondWords = new ArrayList<>(Arrays.asList("tore", "tare"));
        assertEquals(Arrays.asList("tere", "tõre", "tore", "tare"), concat.concat(firstWords, secondWords));
    }

    @Test
    public void concatenatesTwoListsUsingAddAllMethod() {
        List<String> firstWords = new ArrayList<>(Arrays.asList("tere", "tõre"));
        List<String> secondWords = new ArrayList<>(Arrays.asList("tore", "tare"));
        assertEquals(Arrays.asList("tere", "tõre", "tore", "tare"), concat.concatWithAddAll(firstWords, secondWords));
    }
}