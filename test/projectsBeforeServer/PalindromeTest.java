package projectsBeforeServer;

import org.junit.Test;
import projectsBeforeServer.Palindrome;

import static org.junit.Assert.*;

public class PalindromeTest {
    private Palindrome palindrome = new Palindrome();

    @Test
    public void sentenceIsNotPalindrome() {
        assertFalse(palindrome.isPalindrome("Eva, can I see bees in a cave?"));
    }

    @Test
    public void emptyStringIsPalindrome() {
        assertTrue(palindrome.isPalindrome(""));
        assertTrue(palindrome.isPalindrome(" "));
    }

    @Test
    public void oneLetterIsPalindrome() {
        assertTrue(palindrome.isPalindrome("a"));
    }

    @Test
    public void withNumbersIsAlsoPalindrome() {
        assertTrue(palindrome.isPalindrome("ab1ba"));
        assertTrue(palindrome.isPalindrome("12321"));
    }

    @Test
    public void notPalindromeIsNotPalindrome() {
        assertFalse(palindrome.isPalindrome("abcd"));
        assertFalse(palindrome.isPalindrome("12345"));
    }

    @Test
    public void isPalindromeWithRecursion() {
        assertTrue(palindrome.isPalindromeWithRecursion("abba"));
        assertTrue(palindrome.isPalindromeWithRecursion("abcba"));
    }

    @Test
    public void isNotPalindromeWithRecursion() {
        assertFalse(palindrome.isPalindromeWithRecursion("palindroom"));
    }

    @Test
    public void numbersCanAlsoBePalindromeWithRecursion() {
        assertTrue(palindrome.isPalindromeWithRecursion("12321"));
        assertFalse(palindrome.isPalindromeWithRecursion("12345"));
    }

}