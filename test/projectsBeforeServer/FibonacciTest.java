package projectsBeforeServer;

import org.junit.Test;
import projectsBeforeServer.Fibonacci;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.*;

public class FibonacciTest {
    private Fibonacci fibonacci = new Fibonacci();

    @Test
    public void returnsFirstFibonacciNumber() {
        assertEquals(Collections.singletonList(0), fibonacci.fibonacci(1));
    }

    @Test
    public void returnsFirstTwoFibonacciNumbers() {
        assertEquals(Arrays.asList(0, 1), fibonacci.fibonacci(2));
    }

    @Test
    public void returnsFirstThreeFibonacciNumbers() {
        assertEquals(Arrays.asList(0, 1, 1), fibonacci.fibonacci(3));
    }

    @Test
    public void returnsFirstFiveFibonacciNumbers() {
        assertEquals(Arrays.asList(0, 1, 1, 2, 3), fibonacci.fibonacci(5));
    }


    @Test
    public void returnsFirstFifteenFibonacciNumbers() {
        assertEquals(Arrays.asList(0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377), fibonacci.fibonacci(15));
        fibonacci.fibonacci(111);
    }

    @Test
    public void returnsFirstFibonacciNumberWithoutRecursion() {
        assertEquals(Collections.singletonList(0), fibonacci.fibonacciWithoutRecursion(1));
    }

    @Test
    public void returnsFirstTwoFibonacciNumbersWithoutRecursion() {
        assertEquals(Arrays.asList(0, 1), fibonacci.fibonacciWithoutRecursion(2));
    }

    @Test
    public void returnsFirstThreeFibonacciNumbersWithoutRecursion() {
        assertEquals(Arrays.asList(0, 1, 1), fibonacci.fibonacciWithoutRecursion(3));
    }

    @Test
    public void returnsFirstFiveFibonacciNumbersWithoutRecursion() {
        assertEquals(Arrays.asList(0, 1, 1, 2, 3), fibonacci.fibonacciWithoutRecursion(5));
    }


    @Test
    public void returnsFirstFifteenFibonacciNumbersWithoutRecursion() {
        assertEquals(Arrays.asList(0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377), fibonacci.fibonacciWithoutRecursion(15));
        fibonacci.fibonacciWithoutRecursion(111005);
    }

    @Test
    public void returnsFirstFibonacciNumberUsingTailRecursion() {
        assertEquals(Collections.singletonList(0), fibonacci.fibonacciWithTailRecursion(1));
    }

    @Test
    public void returnsFirstTwoFibonacciNumbersUsingTailRecursion() {
        assertEquals(Arrays.asList(0, 1), fibonacci.fibonacciWithTailRecursion(2));
    }

    @Test
    public void returnsFirstThreeFibonacciNumbersUsingTailRecursion() {
        assertEquals(Arrays.asList(0, 1, 1), fibonacci.fibonacciWithTailRecursion(3));
    }

    @Test
    public void returnsFirstFifteenFibonacciNumbersUsingTailRecursion() {
        assertEquals(Arrays.asList(0, 1, 1, 2, 3/*, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377*/), fibonacci.fibonacciWithTailRecursion(5));
        //fibonacci.fibonacciWithTailRecursion(111005);
    }
}