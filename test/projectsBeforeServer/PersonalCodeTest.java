package projectsBeforeServer;

import org.junit.Test;
import projectsBeforeServer.FemaleFirstAndSortByMonth;
import projectsBeforeServer.GenderCompareFemalesFirst;
import projectsBeforeServer.PersonalCode;
import projectsBeforeServer.SortByMonth;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PersonalCodeTest {
    private PersonalCode kairi = new PersonalCode("49006135216");
    private PersonalCode adrian = new PersonalCode("50407150000");
    private PersonalCode debora = new PersonalCode("60610180000");
    private PersonalCode urmas = new PersonalCode("36007061111");
    private PersonalCode olderFemale = new PersonalCode("21904232222");
    private PersonalCode olderMale = new PersonalCode("11904232222");
    private GenderCompareFemalesFirst gc = new GenderCompareFemalesFirst();
    private SortByMonth sbm = new SortByMonth();

    @Test
    public void with246isFemale() {
        assertEquals(PersonalCode.Gender.FEMALE, kairi.getGender());
        assertEquals(PersonalCode.Gender.FEMALE, debora.getGender());
        assertEquals(PersonalCode.Gender.FEMALE, olderFemale.getGender());
    }

    @Test
    public void with135isMale() {
        assertEquals(PersonalCode.Gender.MALE, adrian.getGender());
        assertEquals(PersonalCode.Gender.MALE, urmas.getGender());
        assertEquals(PersonalCode.Gender.MALE, olderMale.getGender());
    }

    @Test
    public void is28on23April2019() {
        assertEquals(28, kairi.getAgeOn(LocalDate.of(2019, Month.APRIL, 23)));
    }

    @Test
    public void is29on13June2019() {
        assertEquals(29, kairi.getAgeOn(LocalDate.of(2019, Month.JUNE, 13)));
    }

    @Test
    public void returnsCorrectDOB() {
        assertEquals(LocalDate.of(1990, Month.JUNE, 13), kairi.getDateOfBirth());
        assertEquals(LocalDate.of(2004, Month.JULY, 15), adrian.getDateOfBirth());
        assertEquals(LocalDate.of(2006, Month.OCTOBER, 18), debora.getDateOfBirth());
        assertEquals(LocalDate.of(1960, Month.JULY, 6), urmas.getDateOfBirth());
        assertEquals(LocalDate.of(1819, Month.APRIL, 23), olderFemale.getDateOfBirth());
        assertEquals(LocalDate.of(1819, Month.APRIL, 23), olderMale.getDateOfBirth());
    }

    @Test
    public void sortsToFemalesFirst() {
        List<PersonalCode> persons = new ArrayList<>(Arrays.asList(kairi, debora, olderFemale, adrian, olderMale, urmas));
        List<PersonalCode> personsToOrder = new ArrayList<>(Arrays.asList(adrian, olderMale, urmas, kairi, debora, olderFemale));
        personsToOrder.sort(gc);
        assertEquals(persons, personsToOrder);
    }

    @Test
    public void sameGenderReturn0() {
        assertEquals(0, gc.compare(kairi, debora));
        assertEquals(0, gc.compare(adrian, olderMale));
    }

    @Test
    public void comparingFemaleToMaleReturnsNegativeInt() {
        assertTrue(gc.compare(kairi, adrian)<0);
    }

    @Test
    public void comparingMaleToFemaleReturnsPositiveInt() {
        assertTrue(gc.compare(adrian, kairi)>0);
    }

    @Test
    public void sortsByMonth() {
        List<PersonalCode> persons = new ArrayList<>(Arrays.asList(olderMale, olderFemale, kairi, adrian, urmas, debora));
        List<PersonalCode> personsToOrder = new ArrayList<>(Arrays.asList(adrian, olderMale, urmas, kairi, debora, olderFemale));
        personsToOrder.sort(sbm);
        assertEquals(persons, personsToOrder);
    }

    @Test
    public void comparingAprilToJuneReturnsNegativeInt() {
        assertTrue(sbm.compare(olderFemale, kairi)<0);
    }

    @Test
    public void comparingJuneToAprilReturnsPositiveInt() {
        assertTrue(sbm.compare(kairi, olderMale)>0);
    }

    @Test
    public void comparingTwoAprilsReturns0() {
        assertEquals(0, sbm.compare(olderFemale, olderMale));
    }

    @Test
    public void comparingNullToNullReturns0ForFemaleFirstAndSortByMonthClassCompare() {
        FemaleFirstAndSortByMonth ffsbm = new FemaleFirstAndSortByMonth();
        assertEquals(0, ffsbm.compare(null, null));
        assertEquals(0, ffsbm.compare(new PersonalCode(null), new PersonalCode(null)));
    }

    @Test
    public void comparingNullToNotNullReturnsPositiveIntForFemaleFirstAndSortByMonthClassCompare() {
        FemaleFirstAndSortByMonth ffsbm = new FemaleFirstAndSortByMonth();
        assertTrue(ffsbm.compare(null, kairi)>0);
        assertTrue(ffsbm.compare(new PersonalCode(null), kairi)>0);
    }

    @Test
    public void comparingNotNullToNullReturnsNegativeIntForFemaleFirstAndSortByMonthClassCompare() {
        FemaleFirstAndSortByMonth ffsbm = new FemaleFirstAndSortByMonth();
        assertTrue(ffsbm.compare(kairi, null)<0);
        assertTrue(ffsbm.compare(kairi, new PersonalCode(null))<0);
    }

    @Test
    public void comparingNullToNullReturns0ForSortByMonth() {
        SortByMonth sbm = new SortByMonth();
        assertEquals(0, sbm.compare(null, null));
        assertEquals(0, sbm.compare(new PersonalCode(null), new PersonalCode(null)));
    }

    @Test
    public void comparingNullToNotNullReturnsPositiveIntForSortByMonth() {
        SortByMonth sbm = new SortByMonth();
        assertTrue(sbm.compare(null, kairi)>0);
        assertTrue(sbm.compare(new PersonalCode(null), kairi)>0);
    }

    @Test
    public void comparingNotNullToNullReturnsNegativeIntForSortByMonth() {
        SortByMonth sbm = new SortByMonth();
        assertTrue(sbm.compare(kairi, null)<0);
        assertTrue(sbm.compare(kairi, new PersonalCode(null))<0);
    }

    @Test
    public void comparingNullToNullReturns0ForGenderCompareFemalesFirst() {
        GenderCompareFemalesFirst gc = new GenderCompareFemalesFirst();
        assertEquals(0, gc.compare(null, null));
        assertEquals(0, gc.compare(new PersonalCode(null), new PersonalCode(null)));
    }

    @Test
    public void comparingNullToNotNullReturnsPositiveIntForGenderCompareFemalesFirst() {
        GenderCompareFemalesFirst gc = new GenderCompareFemalesFirst();
        assertTrue(gc.compare(null, kairi)>0);
        assertTrue(gc.compare(new PersonalCode(null), kairi)>0);
    }

    @Test
    public void comparingNotNullToNullReturnsNegativeIntForGenderCompareFemalesFirst() {
        GenderCompareFemalesFirst gc = new GenderCompareFemalesFirst();
        assertTrue(gc.compare(kairi, null)<0);
        assertTrue(gc.compare(kairi, new PersonalCode(null))<0);
    }
}