package projectsBeforeServer;

import org.junit.Test;
import projectsBeforeServer.DirectoryTreePrinter;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

public class DirectoryTreePrinterTest {

    @Test
    public void printsAccurateTree() {
        String testDirectory = "DirectoryTreeTestFolder\n" +
                "  File two.html\n" +
                "  Internal directory one\n" +
                "    Internal file one.js\n" +
                "    Internal file two.css\n" +
                "  a real project\n" +
                "    index.html\n" +
                "    script.js\n" +
                "    style.css\n" +
                "  File one\n";
        final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        DirectoryTreePrinter.directoryTreePrinter("DirectoryTreeTestFolder");

        assertEquals(testDirectory, outContent.toString());
    }
}