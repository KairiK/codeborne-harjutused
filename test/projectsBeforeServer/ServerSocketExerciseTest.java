package projectsBeforeServer;

import org.junit.Test;
import projectsBeforeServer.ServerSocketExercise;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;


public class ServerSocketExerciseTest {
    private ServerSocketExercise server = new ServerSocketExercise();

    @Test
    public void returnsValidOutputMessageForClient() throws MalformedURLException {
        URL baseUrl = new URL("http://localhost:7777");
        URL testUrl1 = new URL(baseUrl, "kairi");
        URL testUrl2 = new URL(baseUrl, "goodbye");
        assertArrayEquals("HTTP/1.0 200 OK\nContent-Length: 5\n\nkairi".getBytes(), server.headerBuilderForNotFiles(testUrl1, "GET"));
        assertArrayEquals("HTTP/1.0 200 OK\nContent-Length: 7\n\ngoodbye".getBytes(), server.headerBuilderForNotFiles(testUrl2, "GET"));
    }

    @Test
    public void retrievesCorrectReference() {
        List<String> input = new ArrayList<>(Arrays.asList("GET /Piret HTTP/1.1",
                "Host: localhost:7777",
                "Connection: keep-alive",
                "Cache-Control: max-age=0",
                "Upgrade-Insecure-Requests: 1",
                "User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36",
                "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
                "Accept-Encoding: gzip, deflate, br",
                "Accept-Language: en-GB,en-US;q=0.9,en;q=0.8,et;q=0.7"));
        assertEquals("Piret", server.getReference(input));
    }

    @Test
    public void ifContainsPeriodThenIsFile() throws MalformedURLException {
        URL baseUrl = new URL("http://localhost:7777");
        URL testUrl1 = new URL(baseUrl, "kareva.txt");
        URL testUrl2 = new URL(baseUrl, "kareva.html");
        URL testUrl3 = new URL(baseUrl, "kareva");
        assertTrue(server.isReferenceAFile(testUrl1));
        assertTrue(server.isReferenceAFile(testUrl2));
        assertFalse(server.isReferenceAFile(testUrl3));
    }

    @Test
    public void returnsFileType() {
        assertEquals("txt", server.findFileType("kareva.txt"));
        assertEquals("html", server.findFileType("kareva.html"));
    }

    @Test
    public void returnsTrueIfFilesExistOtherwiseFalse() {
        assertTrue(server.doesFileExist("kareva.txt"));
        assertFalse(server.doesFileExist("kareva.jpg"));
    }

    @Test
    public void concatenatesTwoByteArrays() {
        byte[] first = {1, 2, 3};
        byte[] second = {4, 5, 6};
        byte[] bothArrays = {1, 2, 3, 4, 5, 6};
        assertArrayEquals(bothArrays, server.concatenatesByteArrays(first, second));
    }

    @Test
    public void returnsHeaderForKarevaTxtFile() throws IOException {
        String karevaHeaderAsString = "HTTP/1.0 200 OK\nContent-Type: text/plain; charset=utf-8\nContent-Length: 389\n\n";
        assertArrayEquals(karevaHeaderAsString.getBytes(), server.headerBuilderForFiles("kareva.txt"));
    }

    @Test
    public void convertsFileToByteArray() throws IOException {
        String expectedOutput = "<!DOCTYPE html>\n<html lang=\"en\">\n<head>\n    <meta charset=\"UTF-8\">\n    <title>Hello World!</title>\n</head>\n<body>\n<p>Hello world!</p>\n</body>\n</html>";
        assertArrayEquals(expectedOutput.getBytes(), server.fileToByteArray("testfile.html"));
    }

    @Test
    public void writesByteArrayToOutputStream() throws IOException {
        final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

        byte[] informationToSend = {1, 2, 3};
        server.respondToClient(informationToSend, outContent);

        assertArrayEquals(informationToSend, outContent.toByteArray());
    }

    @Test
    public void receivesAdequateHTTPresponseForHeadRequest() throws IOException {
        String expectedResponse = "200 OK\nContent-Length: 31706\nContent-Type: image/jpeg; charset=utf-8\n";

        URL url = new URL("http://localhost:7777/testfile.jpg");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("HEAD");

        StringBuilder fullResponseBuilder = new StringBuilder();

        fullResponseBuilder.append(con.getResponseCode())
                .append(" ")
                .append(con.getResponseMessage())
                .append("\n");

        con.getHeaderFields().entrySet().stream()
                .filter(entry -> entry.getKey() != null)
                .forEach(entry -> {
                    fullResponseBuilder.append(entry.getKey()).append(": ");
                    List<String> headerValues = entry.getValue();
                    for (String header : headerValues) {
                        fullResponseBuilder.append(header);
                    }
                    fullResponseBuilder.append("\n");
                });
        con.disconnect();

        assertEquals(expectedResponse, fullResponseBuilder.toString());
    }
}