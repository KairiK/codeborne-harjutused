package projectsBeforeServer;

import org.junit.Test;
import projectsBeforeServer.SelectionSortWithComparator;
import projectsBeforeServer.ShorterFirst;
import projectsBeforeServer.SmallerFirst;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class SelectionSortWithComparatorTest {
    @Test
    public void returnsOrderedNumbers() {
        SmallerFirst sf = new SmallerFirst();
        List<Integer> integers = new ArrayList<>(Arrays.asList(99, 43, 3, 54, 2777, 4, 1, 0, 597));
        assertEquals(Arrays.asList(0, 1, 3, 4, 43, 54, 99, 597, 2777), SelectionSortWithComparator.sort(integers, sf));
    }

    @Test
    public void returnsWordsShortestToLongest() {
        ShorterFirst shf = new ShorterFirst();
        List<String> words = new ArrayList<>(Arrays.asList("elevant", "koer", "kassid", "ahv", "pikksona"));
        assertEquals(Arrays.asList("ahv", "koer", "kassid", "elevant", "pikksona"), SelectionSortWithComparator.sort(words, shf));
    }
}