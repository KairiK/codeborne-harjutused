package projectsBeforeServer;

import org.junit.Test;
import projectsBeforeServer.IsEven;

import static org.junit.Assert.*;

public class IsEvenTest {
    private IsEven isEven = new IsEven();


    @Test
    public void isEven() {
        assertTrue(isEven.isEven(2));
    }

    @Test
    public void isOdd() {
        assertFalse(isEven.isEven(727379969));
    }

    @Test
    public void zeroIsEven() {
        //noinspection SimplifiableJUnitAssertion
        assertEquals(true, isEven.isEven(0)); //could be simplified
    }

    @Test
    public void isEvenWithoutMod() {
        assertTrue(isEven.isEvenWithoutMod(6));
    }

    @Test
    public void isOddWithoutMod() {
        assertFalse(isEven.isEvenWithoutMod(19));
    }

    @Test
    public void negativeOddIsStillOddWithoutMod() {
        //noinspection SimplifiableJUnitAssertion
        assertEquals("Input number not odd", false, isEven.isEvenWithoutMod(-33333));
        //could be simplified
    }

    @Test
    public void isEvenWithBitwise() {
        assertTrue(isEven.isEvenWithBitwise(2));
    }

    @Test
    public void isOddWithBitWise() {
        assertFalse(isEven.isEvenWithBitwise(99999));
    }

    @Test
    public void is20by6mod4() {
        assertEquals(2, isEven.mod(20, 6));
    }

    @Test
    public void is20by2mod0() {
        assertEquals(0, isEven.mod(20, 2));
    }
}