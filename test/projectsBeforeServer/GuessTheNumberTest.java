package projectsBeforeServer;

import org.junit.Test;
import projectsBeforeServer.GuessTheNumber;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class GuessTheNumberTest {

    @Test
    public void returnsIntInRange() {
        for (int i = 0; i < 1000; i++ ) {
            int number = GuessTheNumber.randomNumberGenerator();
            assertTrue(number < 101);
            assertTrue(number > 0);
        }
    }

    @Test
    public void randomIntsDontRepeat() {
        List<Integer> randomInts = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            int number = GuessTheNumber.randomNumberGenerator();
            System.out.println(number);
            assertFalse(randomInts.contains(number));
            randomInts.add(number);
        }
    }

    @Test
    public void returnsCorrectResponseToInput() {
        assertTrue(GuessTheNumber.assessAnswer(10, 10));
        assertFalse(GuessTheNumber.assessAnswer(10, 5));
        assertFalse(GuessTheNumber.assessAnswer(11, 50));
    }

    @Test
    public void checksAnswerCompliance() {
        assertFalse(GuessTheNumber.checksAnswerCompliance("a"));
        assertFalse(GuessTheNumber.checksAnswerCompliance(""));
        assertFalse(GuessTheNumber.checksAnswerCompliance("exit"));
        assertFalse(GuessTheNumber.checksAnswerCompliance("quit"));
        assertFalse(GuessTheNumber.checksAnswerCompliance("#@"));
        assertTrue(GuessTheNumber.checksAnswerCompliance("10"));
        assertTrue(GuessTheNumber.checksAnswerCompliance("100"));
        assertTrue(GuessTheNumber.checksAnswerCompliance("1000"));
        assertTrue(GuessTheNumber.checksAnswerCompliance("42"));
    }

    @Test
    public void gameOverWhenQuit() {
        assertTrue(GuessTheNumber.isGameOver(10, "quit"));
        assertTrue(GuessTheNumber.isGameOver(10, "QUIT"));
        assertTrue(GuessTheNumber.isGameOver(10, "Quit"));
    }

    @Test
    public void gameOverWhenCorrectAnswer() {
        assertTrue(GuessTheNumber.isGameOver(10, "10"));
        assertTrue(GuessTheNumber.isGameOver(99, "99"));
        assertTrue(GuessTheNumber.isGameOver(1, "1"));
    }

    @Test
    public void gameContinuesWhenWrongAnswer() {
        assertFalse(GuessTheNumber.isGameOver(10, "17"));
        assertFalse(GuessTheNumber.isGameOver(99, "17"));
        assertFalse(GuessTheNumber.isGameOver(1, "17"));
    }

    @Test
    public void gameContinuesWhenNotANumber() {
        assertFalse(GuessTheNumber.isGameOver(10, "q"));
        assertFalse(GuessTheNumber.isGameOver(99, "Foo"));
        assertFalse(GuessTheNumber.isGameOver(1, "Tere"));
    }
}