package projectsBeforeServer;

import org.junit.Test;
import projectsBeforeServer.PigLatin;

import static org.junit.Assert.*;

public class PigLatinTest {
    private PigLatin pigLatin = new PigLatin();

    @Test
    public void wordCanBePigLatinized() {
        assertEquals("atinLay", pigLatin.pigLatinGenerator("Latin"));
        assertEquals("igPay", pigLatin.pigLatinGenerator("Pig"));
    }

    @Test
    public void emptyInputReturnsEmptyString(){
        assertEquals("", pigLatin.pigLatinGenerator(""));
    }

    @Test
    public void singleLetterInputReturnsLetterPigLatinized() {
        assertEquals("Xay", pigLatin.pigLatinGenerator("X"));
    }

    @Test
    public void returnsPigLatinizedSentence() {
        assertEquals("owHay reaay ouyay", pigLatin.pigLatinGenerator("How are you"));
        assertEquals("owHay reaay ou?yay", pigLatin.pigLatinGenerator("How are you?"));
    }

    @Test
    public void wordCanBePigLatinizedWithStream() {
        assertEquals("atinLay", pigLatin.pigLatinGeneratorWithStream("Latin"));
        assertEquals("igPay", pigLatin.pigLatinGeneratorWithStream("Pig"));
    }

    @Test
    public void emptyInputReturnsEmptyStringWithStream(){
        assertEquals("", pigLatin.pigLatinGeneratorWithStream(""));
    }

    @Test
    public void singleLetterInputReturnsLetterPigLatinizedWithStream() {
        assertEquals("Xay", pigLatin.pigLatinGeneratorWithStream("X"));
    }

    @Test
    public void returnsPigLatinizedSentenceWithStream() {
        assertEquals("owHay reaay ouyay", pigLatin.pigLatinGeneratorWithStream("How are you"));
        assertEquals("owHay reaay ou?yay", pigLatin.pigLatinGeneratorWithStream("How are you?"));
    }
}