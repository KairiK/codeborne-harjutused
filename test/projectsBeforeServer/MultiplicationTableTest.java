package projectsBeforeServer;

import org.junit.Test;
import projectsBeforeServer.MultiplicationTable;

import static java.util.Arrays.asList;
import static org.junit.Assert.*;

public class MultiplicationTableTest {
    private MultiplicationTable multiplicationTable = new MultiplicationTable();

    @Test
    public void returns5Times5MultiplicationTable() {

        assertEquals(""+

                "*|  1  2  3  4  5 "+"\n"+
                "------------------"+"\n"+
                "1|  1  2  3  4  5 "+"\n"+
                "2|  2  4  6  8 10 "+"\n"+
                "3|  3  6  9 12 15 "+"\n"+
                "4|  4  8 12 16 20 "+"\n"+
                "5|  5 10 15 20 25", MultiplicationTable.multiplicationTable(5, 5));
    }


    @Test
    public void returns3Times3Multiplication() {
        assertEquals(asList(asList(1, 2, 3), asList(2, 4, 6), asList(3, 6, 9)), MultiplicationTable.returnMultiplicationTableUnformatted(3, 3));
    }

    @Test
    public void calculatesSpaceNeededForColumn() {
        assertEquals(2, MultiplicationTable.bufferFinder(asList(asList(1, 2, 3), asList(2, 4, 6), asList(3, 6, 9)), 1));
    }
}