package projectsBeforeServer;

import org.junit.Test;
import projectsBeforeServer.Stream;

import java.util.Arrays;

import static org.junit.Assert.*;

public class StreamTest {
    private Stream stream = new Stream();

    @Test
    public void returnsDoubleOfList() {
        assertEquals(Arrays.asList(2, 4, 6, 8, 10), stream.returnsDouble(Arrays.asList(1, 2, 3, 4, 5)));
    }

    @Test
    public void returnsDoubleOfListIfInputIsEven() {
        assertEquals(Arrays.asList(4, 8), stream.returnsDoubleIfEven(Arrays.asList(1, 2, 3, 4, 5)));
    }

    @Test
    public void returnsDoubleOfListIfInputIsEvenRemovesRepetitionAndSortsToDecrease() {
        assertEquals(Arrays.asList(20, 16, 8, 4), stream.returnsDoubleIfEvenRemovesRepetitionsAndSortsToDecrease(Arrays.asList(1, 8, 10, 2, 2, 3, 4, 4, 5, 133)));
    }

    @Test
    public void areAllTheseDoubledNumbersDivisibleBy4() {
        assertTrue(stream.isDoubledNumberDivisibleBy4(Arrays.asList(2, 4, 8, 16)));
        assertFalse(stream.isDoubledNumberDivisibleBy4(Arrays.asList(3, 4, 6, 20)));
    }

    @Test
    public void areAnySquaresPrimes() {
        assertFalse(stream.isAnySquareAPrime(Arrays.asList(2, 3, 4, 5, 6, 7, 8, 9, 10)));
    }



}