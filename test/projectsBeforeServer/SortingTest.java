package projectsBeforeServer;

import org.junit.Test;
import projectsBeforeServer.Sorting;

import static org.junit.Assert.*;

public class SortingTest {
    private Sorting sort = new Sorting();

    @Test
    public void returnsSortedList() {
        assertArrayEquals(new int[]{1, 2, 3, 4}, sort.sortToIncrement(new int[]{1, 3, 4, 2}));
        assertArrayEquals(new int[]{0, 2, 4, 6, 8}, sort.sortToIncrement(new int[]{8, 6, 4, 2, 0}));
    }

    @Test
    public void returnsSortedListUsingShortVersion() {
        assertArrayEquals(new int[]{1, 2, 3, 4}, sort.sortToIncrementShortVersion(new int[]{1, 3, 4, 2}));
    }

    @Test
    public void returnsSortedListUsingBubbleSort() {
        assertArrayEquals(new int[]{1, 2, 3, 4, 5, 8, 33, 45, 86}, sort.bubbleSort(new int[]{1, 3, 8, 45, 33, 86, 5, 4, 2}));
    }

    @Test
    public void returnsSortedListUsingBubbleSortWithAlreadySortedInput() {
        assertArrayEquals(new int[]{1, 2, 3, 4, 5, 8, 33, 45, 86}, sort.bubbleSort(new int[]{1, 2, 3, 4, 5, 8, 33, 45, 86}));
    }

    @Test
    public void DoesNotCrashUsingBubbleSortWithEmptyList() {
        assertArrayEquals(new int[]{}, sort.bubbleSort(new int[]{}));
    }

    @Test
    public void DoesNotCrashUsingBubbleSortWithSingleEntryList() {
        assertArrayEquals(new int[]{1}, sort.bubbleSort(new int[]{1}));
    }




}