package projectsBeforeServer;

import org.junit.Test;
import projectsBeforeServer.QuickSortWithComparator;
import projectsBeforeServer.ShorterFirst;
import projectsBeforeServer.SmallerFirst;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class QuickSortWithComparatorTest {
    private SmallerFirst smallerFirstComparator = new SmallerFirst();

    @Test
    public void returnsOrderedNumbersOnly2Nums() {
        List<Integer> integers = new ArrayList<>(Arrays.asList(99, 43));
        assertEquals(Arrays.asList(43, 99), QuickSortWithComparator.sort(integers, smallerFirstComparator));
    }

    @Test
    public void returnsOrderedNumbersOnly3Nums() {
        List<Integer> integers = new ArrayList<>(Arrays.asList(99, 43, 3));
        assertEquals(Arrays.asList(3, 43, 99), QuickSortWithComparator.sort(integers, smallerFirstComparator));
    }

    @Test
    public void returnsOrderedNumbersOnly4Nums() {
        List<Integer> integers = new ArrayList<>(Arrays.asList(99, 43, 3, 54));
        assertEquals(Arrays.asList(3, 43, 54, 99), QuickSortWithComparator.sort(integers, smallerFirstComparator));
    }

    @Test
    public void returnsOrderedNumbers() {
        List<Integer> integers = new ArrayList<>(Arrays.asList(99, 43, 3, 54, 2777, 4, 1, 0, 597));
        assertEquals(Arrays.asList(0, 1, 3, 4, 43, 54, 99, 597, 2777), QuickSortWithComparator.sort(integers, smallerFirstComparator));
    }

    @Test
    public void returnsWordsShortestToLongest() {
        ShorterFirst shf = new ShorterFirst();
        List<String> words = new ArrayList<>(Arrays.asList("elevant", "koer", "kassid", "ahv", "pikksona"));
        assertEquals(Arrays.asList("ahv", "koer", "kassid", "elevant", "pikksona"), QuickSortWithComparator.sort(words, shf));
    }

}