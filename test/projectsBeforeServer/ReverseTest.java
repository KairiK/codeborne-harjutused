package projectsBeforeServer;

import org.junit.Test;
import projectsBeforeServer.Reverse;

import java.util.Arrays;

import static org.junit.Assert.*;

public class ReverseTest {
    private Reverse reverse = new Reverse();

    @Test
    public void returnsReversedList() {
        assertEquals(Arrays.asList("d", "c", "b", "a"), reverse.reverseList(Arrays.asList("a", "b", "c", "d")));
    }

    @Test
    public void returnsReversedListUsingCollections() {
        assertEquals(Arrays.asList("d", "c", "b", "a"), reverse.reverseListWithCollectionsMethod(Arrays.asList("a", "b", "c", "d")));
    }

    @Test
    public void returnsReversedListWithoutLoops() {
        assertEquals(Arrays.asList("d", "c", "b", "a"), reverse.reverseListWithoutLoops(Arrays.asList("a", "b", "c", "d")));
    }
}