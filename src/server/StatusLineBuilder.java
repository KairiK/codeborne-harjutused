package server;

class StatusLineBuilder {
    private ServerUriChecker uriChecker = new ServerUriChecker();

    StatusLine builder(Request request) {
        String httpVersion = request.requestLine.httpVersion;
        String statusPhrase;
        if (uriChecker.uriIsValidAndCanBeResponded(request.requestLine.uri)) {
            //todo statusphrase with enum somehow?
            if(request.requestLine.requestMethod == RequestMethod.POST){
                statusPhrase = "303 See Other";
            }else {
                statusPhrase = "200 OK";
            }
        } else {
            statusPhrase = "404 Not Found";
        }
        return new StatusLine(httpVersion, statusPhrase);
    }
}
