package server;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

class FileParser {

    static byte[] fileToByteArray(String file) {
        try {
            return Files.readAllBytes(Paths.get(new File(file).getPath()));
        } catch (IOException e) {
            System.out.println("File not found");
            e.printStackTrace();
        }
        return new byte[]{};
    }

    static String findFileType(String file) {
        String[] referenceParts = file.split("\\.", 2);
        return referenceParts[1];
    }

    boolean deleteFile(String fileUri){
        File file = new File(fileUri);
        boolean isFileDeleted = file.delete();
        if (isFileDeleted){
            System.out.println("File deleted: " + fileUri);
        }
        return isFileDeleted;
    }

}
