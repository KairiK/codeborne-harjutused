package server;

class StatusLine {
    String httpVersion;
    String StatusPhrase;

    StatusLine(String httpVersion, String statusPhrase) {
        this.httpVersion = httpVersion;
        StatusPhrase = statusPhrase;
    }
}
