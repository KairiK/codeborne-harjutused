package server;

class RequestLine {
    RequestMethod requestMethod;
    ServerUri uri;
    String httpVersion;

    RequestLine(RequestMethod requestMethod, ServerUri uri, String httpVersion) {
        this.requestMethod = requestMethod;
        this.uri = uri;
        this.httpVersion = httpVersion;
    }
}
