package server;

public class ServerQuery {
    String key;
    String value;

    public ServerQuery(String key) {
        this(key, "");
    }

    ServerQuery(String key, String value) {
        this.key = key;
        this.value = value;
    }
}
