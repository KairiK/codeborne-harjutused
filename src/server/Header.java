package server;

class Header {
    String name;
    String value;

    Header(String name, String value) {
        this.name = name;
        this.value = value;
    }
}
