package server;

import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

class HeaderBuilder {
    private ServerUriChecker uriChecker = new ServerUriChecker();

    Header buildContentLength(ServerUri uri) {
        String value;
        if (uriChecker.isReferenceAFile(uri.path)) {
            value = Integer.toString(FileParser.fileToByteArray(uri.path ).length);
        } else {
            NotAFileResponseBody body = new NotAFileResponseBody();
            value = Integer.toString(body.returnResponseBodyAsString(uri).getBytes().length);
        }
        return new Header("Content-Length", value);
    }

    Header buildContentType(ServerUri uri) {
        String value;
        if (uriChecker.isReferenceAFile(uri.path)) {
            if (FileParser.findFileType(uri.path).equalsIgnoreCase("ico")) {
                value = "image/x-icon";
            } else {
                value = URLConnection.guessContentTypeFromName(uri.path) + "; charset=utf-8";
            }
        } else {
            value = "text/plain; charset=utf-8";
        }
        return new Header("Content-Type", value);
    }

    Header buildLocation(){
        return new Header("Location", "/");
    }

    String getHeaderAsString(Header header){
        return header.name + ": " + header.value + "\n";
    }

    List<Header> getHeaderList(Request request, ServerUri uri) {
        List<Header> headers = new ArrayList<>();
        headers.add(buildContentType(uri));
        headers.add(buildContentLength(uri));
        if(request.requestLine.requestMethod == RequestMethod.POST){
            headers.add(buildLocation());
        }
        return headers;
    }
}
