package server;

import java.util.List;

class ServerResponsePOST extends ServerResponse {

    ServerResponsePOST(StatusLine statusLine, List<Header> headers, byte[] messageBody) {
        super(statusLine, headers, messageBody);
    }
}
