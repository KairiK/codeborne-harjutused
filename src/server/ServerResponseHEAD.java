package server;

import java.util.List;

class ServerResponseHEAD extends ServerResponse {

    ServerResponseHEAD(StatusLine statusLine, List<Header> headers) {
        super(statusLine, headers, new byte[]{});
    }
}
