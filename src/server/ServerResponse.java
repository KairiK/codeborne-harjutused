package server;

import java.util.List;

abstract class ServerResponse {
    final StatusLine statusLine;
    final List<Header> headers;
    final byte[] messageBody;

    ServerResponse(StatusLine statusLine, List<Header> headers, byte[] messageBody) {
        this.statusLine = statusLine;
        this.headers = headers;
        this.messageBody = messageBody;
    }

    final byte[] getBytes(){
        HeaderBuilder headerBuilder = new HeaderBuilder();
        ByteArrayConcatenator concat = new ByteArrayConcatenator();

        String statusLineStr = this.statusLine.httpVersion + " " + this.statusLine.StatusPhrase + "\n";
        StringBuilder headers = new StringBuilder();

        for(Header header: this.headers){
            headers.append(headerBuilder.getHeaderAsString(header));
        }
        headers.append("\n");
        String responseAsString = statusLineStr + headers;
        return concat.concatenate(responseAsString.getBytes(), this.messageBody);
    }
}


