package server;

import java.util.ArrayList;
import java.util.List;

class StringParser {

    ServerUri getUriFromString(String uriString) {
        if (uriString.equals("")) {
            return new ServerUri("index.html");
        } else if (!uriString.contains("?")) {
            return new ServerUri(uriString);
        } else {
            String[] uriParts = uriString.split("\\?");
            String path = uriParts[0];
            List<ServerQuery> allQueries = getServerQueries(uriParts);
            return new ServerUri(path, allQueries);
        }
    }

    private List<ServerQuery> getServerQueries(String[] uriParts) {
        List<ServerQuery> allQueries = new ArrayList<>();
        if (uriParts.length > 1) {
            String[] queryParamsAsStringArray = uriParts[1].split("&");
            for (String s : queryParamsAsStringArray) {
                if (!s.equals("")) {
                    String[] queryParts = s.split("=");
                    if (queryParts[1] == null) {
                        queryParts[1] = "";
                    }
                    ServerQuery query = new ServerQuery(queryParts[0], queryParts[1]);
                    allQueries.add(query);
                }
            }
        }
        return allQueries;
    }

    RequestMethod getMethodFromString(String methodString) {
        return RequestMethod.valueOf(methodString.toUpperCase());
    }

    Header getHeaderFromString(String headerString) {
        String[] headerAsStringArray = headerString.split(":", 2);
        String name = headerAsStringArray[0].trim();
        String value = headerAsStringArray[1].trim();
        return new Header(name, value);
    }

    //todo kind of similar to getServerQueries
    RequestBody getRequestBodyFromString(String body) {
        if(!body.equals("")) {
            String[] requestParamsAsStringArray = body.split("&");
            String[] nameAndLastname = new String[2];
            for (int i = 0; i < requestParamsAsStringArray.length; i++) {
                if (!requestParamsAsStringArray[i].equals("")) {
                    String[] requestParts = requestParamsAsStringArray[i].split("=");
                    if (requestParts[1] == null) {
                        requestParts[1] = "";
                    }
                    nameAndLastname[i] = requestParts[1];
                }
            }
            return new RequestBody(nameAndLastname[0], nameAndLastname[1]);
        } else {
            return new RequestBody();
        }
    }
}
