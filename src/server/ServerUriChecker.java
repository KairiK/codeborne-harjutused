package server;

import java.io.File;

class ServerUriChecker {

    boolean uriIsValidAndCanBeResponded(ServerUri uri){
        if(!isReferenceAFile(uri.path)){
            return true;
        }else{
            return doesFileExist(uri.path);
        }
    }

    boolean isReferenceAFile(String path) {
        if ("".equals(path)) {
            return false;
        } else {
            return path.contains(".");
            //todo
            // potential problem: what if aiki.trumm is not a file with .trumm extension but a separate page
        }
    }

    boolean doesFileExist(String path) {
        return new File(path).exists();
    }

    boolean isFileInAllowedDirectory(ServerUri uri){
        return !uri.path.contains("/");
    }

}
