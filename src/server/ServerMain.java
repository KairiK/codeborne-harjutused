package server;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class ServerMain {

    public static void main(String[] args) {
        ServerMain server = new ServerMain();
        starter(7755);
    }

    private static void starter(@SuppressWarnings("SameParameterValue") int port) {
        try {
            ServerSocket serverSocket = new ServerSocket(port);
            System.out.println("Listening for connection on port " + port);

            //noinspection InfiniteLoopStatement
            while (true) {
                try {
                    Socket clientSocket = serverSocket.accept();
                    System.out.println("Connection opened to " + clientSocket.getRemoteSocketAddress());

                    InputStream inputStream = clientSocket.getInputStream();
                    InputStreamReader reader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
                    BufferedReader inReader = new BufferedReader(reader);

                    InputReader inputReader = new InputReader();
                    List<String> allInputLines = inputReader.readAllInput(inReader);

                    RequestParser requestParser = new RequestParser();
                    Request request = requestParser.parseInputLinesToRequest(allInputLines);

                    ServerResponseBuilder responseBuilder = new ServerResponseBuilder();
                    byte[] responseToClient = responseBuilder.buildFullResponse(request).getBytes();

                    OutputStream outputStream = clientSocket.getOutputStream();
                    outputStream.write(responseToClient);

                    outputStream.close();
                    inputStream.close();
                    reader.close();
                    inReader.close();
                    clientSocket.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Unable to run server");
        }
    }
}
