package server;

import java.util.ArrayList;
import java.util.List;

class ServerUri {
    String path;
    List<ServerQuery> queries;

    ServerUri() {
        this("", new ArrayList<>());
    }

    ServerUri(String path) {
        this(path, new ArrayList<>());
    }

    ServerUri(String path, List<ServerQuery> queries) {
        this.path = path;
        this.queries = queries;
    }
}
