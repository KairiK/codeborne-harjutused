package server;

import java.util.List;

class Request {
    RequestLine requestLine;
    List<Header> headers;
    RequestBody requestBody;

    Request(RequestLine requestLine, List<Header> headers, RequestBody requestBody) {
        this.requestLine = requestLine;
        this.headers = headers;
        this.requestBody = requestBody;
    }

    Request(RequestLine requestLine, List<Header> headers) {
        this(requestLine, headers, new RequestBody("", ""));
    }
}
