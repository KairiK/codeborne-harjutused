package server;

import java.io.*;
import java.nio.charset.StandardCharsets;

class LogSubmissions {

    void log(RequestBody requestBody) {
//        try{
        String loggableRequestBody = buildString(requestBody);
        try (FileWriter writer = new FileWriter("log.txt", true);
//        BufferedWriter bufferedWriter = new BufferedWriter
//                (new OutputStreamWriter(new FileOutputStream("log.txt"), StandardCharsets.UTF_8));
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {
        bufferedWriter.write(loggableRequestBody);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String buildString(RequestBody requestBody) {
        return "First name: " + requestBody.firstName + ", last name: " + requestBody.lastName + ";\n";
    }

}
