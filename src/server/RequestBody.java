package server;

class RequestBody {
    String firstName;
    String lastName;

    RequestBody(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    RequestBody() {
        this("", "");
    }
}
