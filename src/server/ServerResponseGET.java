package server;

import java.util.List;

class ServerResponseGET extends ServerResponse {

    ServerResponseGET(StatusLine statusLine, List<Header> headers, byte[] messageBody) {
        super(statusLine, headers, messageBody);
    }
}
