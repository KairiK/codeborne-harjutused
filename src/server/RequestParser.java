package server;

import java.util.ArrayList;
import java.util.List;

class RequestParser {

    Request parseInputLinesToRequest(List<String> inputLines) {
        StringParser stringParser = new StringParser();
        String[] requestLineAsStringArray = inputLines.get(0).split(" ");
        RequestLine requestLine = getRequestLine(stringParser, requestLineAsStringArray);
        List<Header> headers = getHeaders(inputLines, stringParser);
        RequestBody requestBody = getBody(inputLines, stringParser);
        return new Request(requestLine, headers, requestBody);
    }

    private RequestBody getBody(List<String> inputLines, StringParser stringParser) {
        InputReader reader = new InputReader();
        if(reader.getContentLengthIfExists(inputLines)>0) {
            String body = inputLines.get(inputLines.size() - 1);
            return stringParser.getRequestBodyFromString(body);
        } else {
            return new RequestBody("", "");
        }
    }

    private List<Header> getHeaders(List<String> inputLines, StringParser stringParser) {
        List<Header> headers = new ArrayList<>();
        for (int i = 1; i < inputLines.size(); i++) {
            if (!inputLines.get(i).equals("")) {
                Header header = stringParser.getHeaderFromString(inputLines.get(i));
                headers.add(header);
            } else {
                break;
            }
        }
        return headers;
    }

    private RequestLine getRequestLine(StringParser stringParser, String[] requestLineAsStringArray) {
        String requestMethod = requestLineAsStringArray[0];
        String uri = requestLineAsStringArray[1].substring(1);
        String httpVersion = requestLineAsStringArray[2];
        return new RequestLine(stringParser.getMethodFromString(requestMethod), stringParser.getUriFromString(uri), httpVersion);
    }
}
