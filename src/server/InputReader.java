package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

class InputReader {

    List<String> readAllInput(BufferedReader inReader) throws IOException {
        List<String> allInputLines = new ArrayList<>();
        String inputLine;
        StringBuilder body = new StringBuilder();
        while ((inputLine = inReader.readLine()) != null) {
            System.out.println(inputLine);
            allInputLines.add(inputLine);
            if (inputLine.length() == 0) {
                int contentLength = getContentLengthIfExists(allInputLines);
                if (contentLength > 0) {
                    char[] messageBody = new char[contentLength];
                    int numOfCharsReadIn = inReader.read(messageBody, 0, messageBody.length);
                    while (numOfCharsReadIn < messageBody.length) {
                        numOfCharsReadIn = inReader.read(messageBody, numOfCharsReadIn, messageBody.length);
                    }

                    for (char c : messageBody) {
                        body.append(c);
                    }
                }
                break;
            }
        }
        //noinspection CharsetObjectCanBeUsed
        System.out.println(URLDecoder.decode(body.toString(), "UTF-8"));
        allInputLines.add(body.toString());
        return allInputLines;
    }

    int getContentLengthIfExists(List<String> inputLines) {
        int contentLength = 0;
        for (String line : inputLines) {
            if (line.toLowerCase().startsWith("content-length")) {
                String[] lineParts = line.split(":", 2);
                contentLength = Integer.parseInt(lineParts[1].trim());
            }
        }
        return contentLength;
    }
}
