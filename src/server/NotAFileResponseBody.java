package server;

class NotAFileResponseBody {

    String returnResponseBodyAsString(ServerUri uri) {
        if ("goodbye".equalsIgnoreCase(uri.path)) {
            return returnGoodbyeResponse();
        } else if ((uri.queries.isEmpty())) {
            return returnHelloNameResponse(uri.path);
        } else {
            return returnPathWithQueryStringResponse(uri);
        }
    }

    String returnGoodbyeResponse() {
        return "Goodbye";
    }

    String returnHelloNameResponse(String name) {
        return "Hello " + name;
    }

    String returnPathWithQueryStringResponse(ServerUri uri) {
        return uri.path + " " + uri.queries.get(0).value;
    }
}
