package server;

import java.util.List;

class ServerResponseBuilder {
    private ServerUriChecker uriChecker = new ServerUriChecker();
    private StatusLineBuilder statusLineBuilder = new StatusLineBuilder();
    private HeaderBuilder headerBuilder = new HeaderBuilder();
    private FileParser fileParser = new FileParser();

    ServerResponse buildFullResponse(Request request) {
        StatusLine statusLine = statusLineBuilder.builder(request);

        ServerUri uriToUse;
        if (uriChecker.uriIsValidAndCanBeResponded(request.requestLine.uri)) {
            uriToUse = request.requestLine.uri;
        } else {
            uriToUse = new ServerUri("404page.png");
        }
        List<Header> headers = headerBuilder.getHeaderList(request, uriToUse);
        if (request.requestLine.requestMethod == RequestMethod.GET) {
            byte[] messageBody;
            if (uriChecker.uriIsValidAndCanBeResponded(request.requestLine.uri) && !uriChecker.isReferenceAFile(request.requestLine.uri.path)) {
                NotAFileResponseBody body = new NotAFileResponseBody();
                messageBody = body.returnResponseBodyAsString(request.requestLine.uri).getBytes();
            } else {
                messageBody = FileParser.fileToByteArray(uriToUse.path);
            }
            return new ServerResponseGET(statusLine, headers, messageBody);
        } else if (request.requestLine.requestMethod == RequestMethod.POST) {
            LogSubmissions logger = new LogSubmissions();
            logger.log(request.requestBody);

            byte[] messageBody = FileParser.fileToByteArray(uriToUse.path);
            return new ServerResponsePOST(statusLine, headers, messageBody);
        } else if (request.requestLine.requestMethod == RequestMethod.HEAD){
            return new ServerResponseHEAD(statusLine, headers);
        } else {
            if(uriChecker.isFileInAllowedDirectory(uriToUse)&& !uriToUse.path.equals("404page.png")){
                if(fileParser.deleteFile(uriToUse.path)){
                    return new ServerResponseDELETE(statusLine);
                }
            }
            return new ServerResponseDELETE(statusLine);
        }
    }
}
