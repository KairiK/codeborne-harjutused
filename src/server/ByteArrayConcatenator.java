package server;

import java.util.Arrays;

class ByteArrayConcatenator {

    byte[] concatenate(byte[] a, byte[] b) {
        int lenA = a.length;
        int lenB = b.length;
        byte[] completeArray = Arrays.copyOf(a, lenA + lenB);
        System.arraycopy(b, 0, completeArray, lenA, lenB);
        return completeArray;
    }
}
