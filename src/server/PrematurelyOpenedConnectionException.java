package server;

public class PrematurelyOpenedConnectionException extends Exception {

    public PrematurelyOpenedConnectionException(String errorMessage, Throwable err){
        super(errorMessage, err);
    }

}
