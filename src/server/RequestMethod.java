package server;

public enum RequestMethod {
    GET,
    HEAD,
    POST,
    DELETE
}
