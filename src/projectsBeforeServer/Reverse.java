package projectsBeforeServer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class Reverse {
    List<String> reverseList(List<String> inputList){
        List<String> outputList = new ArrayList<>();
        for (int i = inputList.size()-1; i >= 0; i--) {
            outputList.add(inputList.get(i));
        }
        return outputList;
    }

    List<String> reverseListWithCollectionsMethod(List<String> inputList){
        List<String> outputList = new ArrayList<>(inputList);
        Collections.reverse(outputList);
        return outputList;
    }

    List<String> reverseListWithoutLoops(List<String> inputList){
        if(inputList.size()==1){
            return inputList;
        } else {
            List<String> out = new ArrayList<>(reverseListWithoutLoops(inputList.subList(1, inputList.size())));
            out.add(inputList.get(0));
            return out;
        }
    }
}
