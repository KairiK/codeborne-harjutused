package projectsBeforeServer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MultiplicationTable {

    static String multiplicationTable(int row, int column){
        StringBuilder fullMultiplicationTable = new StringBuilder();

        //get necessary width for first and other columns
        int bracketWidth = Integer.toString(row*column).length();
        int firstColumnBracketWidth = Integer.toString(column).length();

        //create first row
        StringBuilder firstRow = new StringBuilder("*");
            firstRow.append(String.join("", Collections.nCopies(firstColumnBracketWidth-1, " "))).append("| ");


        for (int i = 1; i <= column; i++) {
            String buffer = "";
            if(bracketWidth>1){
                int howManySpacesNeeded = bracketWidth-Integer.toString(i).length();
                buffer = String.join("", Collections.nCopies(howManySpacesNeeded, " "));
            }
            firstRow.append(buffer).append(i).append(" ");
        }

        fullMultiplicationTable.append(firstRow).append("\n").append(String.join("", Collections.nCopies(firstRow.length(), "-"))).append("\n");

        //create table content line by line
        for (int i = 0; i < row; i++) {
            List<Integer> rowNumbers = new ArrayList<>();
            for (int j = 0; j < column; j++) {
                rowNumbers.add((i+1)*(j+1));
            }

            StringBuilder rowNumbersStr = new StringBuilder();
            String firstColumnBuffer = "";
                if(firstColumnBracketWidth>1){
                    int howManySpacesNeededFirstColumn = firstColumnBracketWidth-Integer.toString(i+1).length();
                    firstColumnBuffer = String.join("", Collections.nCopies(howManySpacesNeededFirstColumn, " "));
                }
            rowNumbersStr.append(i + 1).append(firstColumnBuffer).append("| ");

            for (int bracketNumber: rowNumbers) {
                String buffer = "";
                if(bracketWidth>1){
                    int howManySpacesNeeded = bracketWidth-Integer.toString(bracketNumber).length();
                    buffer = String.join("", Collections.nCopies(howManySpacesNeeded, " "));
                }
                rowNumbersStr.append(buffer).append(bracketNumber).append(" ");
            }
            fullMultiplicationTable.append(rowNumbersStr).append("\n");
        }
        return fullMultiplicationTable.substring(0, fullMultiplicationTable.length()-2);
    }

//----------------------------------------------------//


    private static StringBuilder multiplicationTableWithDifferentFormatting(int row, int column){

        System.out.println("###########################################");

        List<List<Integer>> tableContent = returnMultiplicationTableUnformatted(row, column);
        StringBuilder formattedTable = new StringBuilder();
        StringBuilder firstLine = firstLineBuilder(row, tableContent, formattedTable);
        secondLineBuilder(formattedTable, firstLine);
        contentFormatter(tableContent, formattedTable);

        return formattedTable;
    }

    static List<List<Integer>> returnMultiplicationTableUnformatted(int row, int column) {
        List<List<Integer>> tableContent = new ArrayList<>();
        for (int i = 1; i <= row; i++) {
            List<Integer> oneRow = new ArrayList<>();
            for (int j = 1; j <= column ; j++) {
                oneRow.add((i*j));
            }
            tableContent.add(oneRow);
        }
        return tableContent;
    }

    static int bufferFinder(List<List<Integer>> inputList, int columnNumber){
        return Integer.toString(inputList.get(inputList.size()-1).get(columnNumber)).length() + 1;
    }

    private static StringBuilder firstLineBuilder(int row, List<List<Integer>> tableContent, StringBuilder formattedTable) {
        StringBuilder firstLine = new StringBuilder();
        firstLine.append(String.join("", Collections.nCopies(bufferFinder(tableContent, 0)-1, " "))).append("*|");

        for(int n : tableContent.get(0)){
            String buffer = String.join("", Collections.nCopies(bufferFinder(tableContent, n-1)-Integer.toString(n).length(), " "));
            firstLine.append(buffer).append(n);
        }
        formattedTable.append(firstLine).append("\n");
        return firstLine;
    }

    private static void secondLineBuilder(StringBuilder formattedTable, StringBuilder firstLine) {
        String secondLine = String.join("", Collections.nCopies(firstLine.length(), "-"));
        formattedTable.append(secondLine).append("\n");
    }

    private static void contentFormatter(List<List<Integer>> tableContent, StringBuilder formattedTable) {
        for (List<Integer> rowOfIntegers : tableContent) {
            StringBuilder line = new StringBuilder();
            line.append(String.join("", Collections.nCopies(bufferFinder(tableContent, 0)-Integer.toString(rowOfIntegers.get(0)).length(), " "))).append(rowOfIntegers.get(0)).append("|");
            for (int i = 0; i<rowOfIntegers.size(); i++) {
                line.append(String.join("", Collections.nCopies(bufferFinder(tableContent, i)-Integer.toString(rowOfIntegers.get(i)).length(), " "))).append(rowOfIntegers.get(i));
            }
            formattedTable.append(line).append("\n");
        }
    }

    public static void main(String[] args) {
        System.out.println(multiplicationTable(10, 10));
        System.out.println(multiplicationTableWithDifferentFormatting(10, 10));
    }
}
