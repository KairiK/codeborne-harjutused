package projectsBeforeServer;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class HeadRequest {

    public static void main(String[] args) throws IOException {

        URL url = new URL("http://localhost:7777/testfile.jpg");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("HEAD");

        StringBuilder fullResponseBuilder = new StringBuilder();

        fullResponseBuilder.append(con.getResponseCode())
                .append(" ")
                .append(con.getResponseMessage());

        System.out.println(fullResponseBuilder.toString());
        fullResponseBuilder.delete(0, fullResponseBuilder.length());

        con.getHeaderFields().entrySet().stream()
                .filter(entry -> entry.getKey() != null)
                .forEach(entry -> {
                    fullResponseBuilder.append(entry.getKey()).append(": ");
                    List<String> headerValues = entry.getValue();
                    for(String header: headerValues){
                        fullResponseBuilder.append(header);
                    }
                    System.out.println(fullResponseBuilder.toString());
                    fullResponseBuilder.delete(0, fullResponseBuilder.length());
                });

        con.disconnect();
    }

}
