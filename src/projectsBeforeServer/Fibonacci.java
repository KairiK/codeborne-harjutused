package projectsBeforeServer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

class Fibonacci {

    List<Integer> fibonacci(int numbersCount) {
        if (numbersCount == 1) {
            return Collections.singletonList(0);
        } else if (numbersCount == 2) {
            return Arrays.asList(0, 1);
        } else {
            List<Integer> fibonacciNumbers = new ArrayList<>(fibonacci(numbersCount - 1));
            fibonacciNumbers.add(fibonacciNumbers.get(fibonacciNumbers.size() - 2) + fibonacciNumbers.get(fibonacciNumbers.size() - 1));
            return fibonacciNumbers;
        }
    }

    List<Integer> fibonacciWithoutRecursion(int numbersCount) {
        if (numbersCount == 1) {
            return new ArrayList<>(Collections.singletonList(0));
        } else if (numbersCount == 2) {
            return new ArrayList<>(Arrays.asList(0, 1));
        } else {
            List<Integer> fibonacciNumbers = new ArrayList<>(Arrays.asList(0, 1));
            while (numbersCount - 2 > 0) {
                fibonacciNumbers.add(fibonacciNumbers.get(fibonacciNumbers.size() - 2) + fibonacciNumbers.get(fibonacciNumbers.size() - 1));
                numbersCount--;
            }
            return fibonacciNumbers;
        }
    }

    private List<Integer> fibonacciWithTailRecursion(int numbersCount, int placeIndicator, List<Integer> fibonacciSequence) {
        if (placeIndicator == numbersCount) {
            return fibonacciSequence;
        }
        fibonacciSequence.set(placeIndicator, (fibonacciSequence.get(placeIndicator - 1) + (fibonacciSequence.get(placeIndicator - 2))));
        return fibonacciWithTailRecursion(numbersCount, placeIndicator + 1, fibonacciSequence);
    }

    List<Integer> fibonacciWithTailRecursion(int numbersCount) {
        if (numbersCount == 1) {
            return Collections.singletonList(0);
        }
        if (numbersCount == 2) {
            return Arrays.asList(0, 1);
        }
        List<Integer> fibonacciSequence = new ArrayList<>();
        for (int i = 0; i < numbersCount; i++) {
            fibonacciSequence.add(0);
        }
        fibonacciSequence.set(0, 0);
        fibonacciSequence.set(1, 1);
        return fibonacciWithTailRecursion(numbersCount, 2, fibonacciSequence);
    }

    private static int returnsNthFibonacciNumberWithTailRecursion(int numbersCount, int resultIf1, int resultIf2) {
        if (numbersCount == 0)
            return resultIf1;
        if (numbersCount == 1)
            return resultIf2;
        return returnsNthFibonacciNumberWithTailRecursion(numbersCount - 1, resultIf2, resultIf1 + resultIf2);
    }

    public static void main(String[] args) {
        int n = 9;
        System.out.println("fib(" + n + ") = " + returnsNthFibonacciNumberWithTailRecursion(n, 0, 1));
        System.out.println(factorial(5));
        System.out.println(factorialWithTailRecursion(5));
    }

    static int factorial(int inputNumber) {
        if (inputNumber <= 1) {
            return 1;
        }
        return factorial(inputNumber - 1) * inputNumber;
    }

    static int factorialWithTailRecursion(int inputNumber, int accum) {
        if (inputNumber <= 1) {
            return accum;
        }
        return factorialWithTailRecursion(inputNumber - 1, accum * inputNumber);
    }

    static int factorialWithTailRecursion(int inputNumber) {
        return factorialWithTailRecursion(inputNumber, 1);
    }
}

