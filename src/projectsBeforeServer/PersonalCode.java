package projectsBeforeServer;

import java.time.LocalDate;
import java.time.Month;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

class PersonalCode implements Comparable<PersonalCode> {

    String personalCode;

    PersonalCode(String personalCode) {
        this.personalCode = personalCode;
    }

    @Override
    public int compareTo(PersonalCode o) {
        return this.personalCode.compareTo(o.personalCode);
    }

    LocalDate getDateOfBirth() {
        int year;

        int century = Integer.parseInt(personalCode.substring(0, 1));
        int birthYear = Integer.parseInt(personalCode.substring(1, 3));
        if (century == 3 || century == 4) {
            year = 1900 + birthYear;
        } else if (century == 5 || century == 6) {
            year = 2000 + birthYear;
        } else {
            year = 1800 + birthYear;
        }

        int month = Integer.parseInt(personalCode.substring(3, 5));
        int day = Integer.parseInt(personalCode.substring(5, 7));
        return LocalDate.of(year, month, day);
    }

    int getAgeOn(LocalDate date) {
        return (int) ChronoUnit.YEARS.between(getDateOfBirth(), date);
    }

    Gender getGender() {
        HashSet<Integer> male = new HashSet<>(Arrays.asList(1, 3, 5));

        if (male.contains(Integer.parseInt(personalCode.substring(0, 1)))) {
            return Gender.MALE;
        } else {
            return Gender.FEMALE;
        }
    }

    Month getMonth() {
        return getDateOfBirth().getMonth();
    }

    enum Gender {
        MALE, FEMALE
    }
}

class GenderCompareFemalesFirst implements Comparator<PersonalCode> {
    @Override
    public int compare(PersonalCode o1, PersonalCode o2) {
        if (o1 == null && o2 == null) {
            return 0;
        } else if (o1 == null) {
            return 1;
        } else if (o2 == null) {
            return -1;
        } else if (o1.personalCode == null && o2.personalCode == null) {
            return 0;
        } else if (o1.personalCode == null) {
            return 1;
        } else if (o2.personalCode == null) {
            return -1;
        } else {
            if (o1.getGender() == o2.getGender()) {
                return 0;
            } else if (o1.getGender() == PersonalCode.Gender.FEMALE && o2.getGender() == PersonalCode.Gender.MALE) {
                return -1;
            }
            return 1;
        }
    }
}

class SortByMonth implements Comparator<PersonalCode> {
    @Override
    public int compare(PersonalCode o1, PersonalCode o2) {
        if (o1 == null && o2 == null) {
            return 0;
        } else if (o1 == null) {
            return 1;
        } else if (o2 == null) {
            return -1;
        } else if (o1.personalCode == null && o2.personalCode == null) {
            return 0;
        } else if (o1.personalCode == null) {
            return 1;
        } else if (o2.personalCode == null) {
            return -1;
        } else {
            return Integer.compare(o1.getDateOfBirth().getMonthValue(), o2.getDateOfBirth().getMonthValue());
        }
    }
}

class FemaleFirstAndSortByMonth implements Comparator<PersonalCode> {
    List<PersonalCode> sortByGenderAndMonth(List<PersonalCode> persons) {
        Comparator<PersonalCode> sortByGender_nullsLast = Comparator.nullsLast(Comparator.comparing(PersonalCode::getGender).reversed());
        Comparator<PersonalCode> sortByMonth_nullsLast = Comparator.nullsLast(Comparator.comparing(PersonalCode::getMonth));
        return persons.stream()
                .sorted(sortByGender_nullsLast.thenComparing(sortByMonth_nullsLast))
                .collect(Collectors.toList());
    }

    @Override
    public int compare(PersonalCode o1, PersonalCode o2) {
        if (o1 == null && o2 == null) {
            return 0;
        } else if (o1 == null) {
            return 1;
        } else if (o2 == null) {
            return -1;
        } else if (o1.personalCode == null && o2.personalCode == null) {
            return 0;
        } else if (o1.personalCode == null) {
            return 1;
        } else if (o2.personalCode == null) {
            return -1;
        } else {
            int c;
            c = o2.getGender().compareTo(o1.getGender());
            if (c == 0) {
                c = o1.getMonth().compareTo(o2.getMonth());
            }
            return c;
        }
    }
}