package projectsBeforeServer;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class AdventOfCode {

    public static void main(String[] args) throws FileNotFoundException {
        List<Integer> frequencyChanges = inputReader();
        List<Integer> longListOfFrequencyChanges = new ArrayList<>();
        longListOfFrequencyChanges.addAll(frequencyChanges);

        int currentFrequency = 0;
        List<Integer> allResultingFrequencies = new ArrayList<>();

        while(!isNumRepeated(currentFrequency, allResultingFrequencies)){
            for (int i: longListOfFrequencyChanges) {
                currentFrequency+=i;
                if(isNumRepeated(currentFrequency, allResultingFrequencies)){
                    System.out.println(currentFrequency);
                }
                allResultingFrequencies.add(currentFrequency);
                longListOfFrequencyChanges.addAll(frequencyChanges);
            }
        }
        System.out.println(allResultingFrequencies);
    }

    static boolean isNumRepeated(int i, List<Integer> list){
        return list.contains(i);
    }

    static List<Integer> inputReader() throws FileNotFoundException {
        List<Integer> inputAsList = new ArrayList<>();
        File file = new File("adventOfCodeInput.txt");
        Scanner fileScanner = new Scanner(file);
        while(fileScanner.hasNextLine()){
            String line = fileScanner.nextLine();
            inputAsList.add(Integer.valueOf(line));
        }
        return inputAsList;
    }

}
