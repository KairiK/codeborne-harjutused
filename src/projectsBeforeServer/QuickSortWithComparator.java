package projectsBeforeServer;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

class QuickSortWithComparator {
        static <T> List<T> sort(List<T> inputList, Comparator<T> comparator) {
        if (inputList.size() <= 1) {
            return inputList;
        } else {
            T pivot = inputList.get(0);
            List<T> left = new ArrayList<>();
            List<T> right = new ArrayList<>();

            for (T element : inputList) {
                if(comparator.compare(pivot, element) < 0){
                    right.add(element);
                }else if(comparator.compare(pivot, element) > 0){
                    left.add(element);
                }
            }
            List<T> firstHalf = sort(left, comparator);
            List<T> secondHalf = sort(right, comparator);
            firstHalf.add(pivot);
            firstHalf.addAll(secondHalf);
            return firstHalf;
        }
    }
}
