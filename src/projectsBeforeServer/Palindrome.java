package projectsBeforeServer;

class Palindrome {
    boolean isPalindrome(String phrase){
        boolean result = true;
        int inputWordLength = phrase.length();
        if(inputWordLength%2==0){
            for (int i = 0; i < inputWordLength/2; i++) {
                if(!phrase.substring(i, i+1).equals(phrase.substring(phrase.length()-1-i, phrase.length()-i))){
                    result = false;
                    break;
                }
            }
        }else{
            for (int i = 0; i < (inputWordLength-1)/2; i++) {
                if(!phrase.substring(i, i+1).equals(phrase.substring(phrase.length()-1-i, phrase.length()-i))){
                    result = false;
                    break;
                }
            }
        }
        return result;
    }

    boolean isPalindromeWithRecursion(String phrase){
        if(phrase.length()<=1){
            return true;
        }else{
            boolean result = isPalindromeWithRecursion(phrase.substring(1, phrase.length()-1));
            if(!result){
                return result;
            }else{
                return phrase.substring(0, 1).equals(phrase.substring(phrase.length() - 1));
            }
        }
    }
}
