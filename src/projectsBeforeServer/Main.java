package projectsBeforeServer;

import java.util.*;

public class Main {

    public static void main(String[] args) {

        System.out.println("ArrayList: ");
        List<PersonalCode> v = new ArrayList<>();
        v.add(new PersonalCode("49006131000"));
        v.add(new PersonalCode("38106180000"));
        v.add(new PersonalCode("49006130100"));
        v.add(new PersonalCode("60610180000"));
        v.add(new PersonalCode("49006130000"));
        v.add(new PersonalCode("49006130001"));

        for (PersonalCode i: v) {
            System.out.println(i.personalCode);
        }
        Collections.sort(v);
        System.out.println("projectsBeforeServer.Sorting ArrayList...");
        for (PersonalCode i: v) {
            System.out.println(i.personalCode);
        }

        System.out.println("Sort females first: ");

        GenderCompareFemalesFirst genderCompare = new GenderCompareFemalesFirst();
        v.sort(genderCompare);
        for (PersonalCode i: v) {
            System.out.println(i.personalCode);
        }


        PersonalCode kairi = new PersonalCode("49006135216");
        PersonalCode adrian = new PersonalCode("50407150000");
        PersonalCode debora = new PersonalCode("60610180000");
        PersonalCode urmas = new PersonalCode("36007061111");
        PersonalCode olderFemale = new PersonalCode("21904232222");
        PersonalCode olderMale = new PersonalCode("11904232222");
        List<PersonalCode> persons = new ArrayList<>(Arrays.asList(olderMale, null, olderFemale, adrian, urmas, debora));
        List<PersonalCode> personsDuplicate = new ArrayList<>(Arrays.asList(olderMale, olderFemale, new PersonalCode(null), kairi, adrian, urmas, debora, new PersonalCode(null)));

        System.out.println("Sort by gender and month");
        FemaleFirstAndSortByMonth ffsbm = new FemaleFirstAndSortByMonth();
        List<PersonalCode> sorted = ffsbm.sortByGenderAndMonth(persons);
        System.out.println(sorted);
        for (PersonalCode i: sorted) {
           System.out.println(i.personalCode);
        }
        System.out.println("Before sorting with null");
        System.out.println(personsDuplicate);
        System.out.println("Testing with overridden compare method: ");
        System.out.println("Sort by gender and month");
        personsDuplicate.sort(ffsbm);
        for (PersonalCode i: personsDuplicate) {
            System.out.println(i.personalCode);
        }
        System.out.println(personsDuplicate);

        System.out.println("Comparing genders with null projectsBeforeServer.PersonalCode: ");
        personsDuplicate.sort(new GenderCompareFemalesFirst());
        for (PersonalCode i: personsDuplicate) {
            System.out.println(i.personalCode);
        }

        System.out.println("Comparing months with null projectsBeforeServer.PersonalCode: ");
        personsDuplicate.sort(new SortByMonth());
        for (PersonalCode i: personsDuplicate) {
            System.out.println(i.personalCode);
        }
    }
}
