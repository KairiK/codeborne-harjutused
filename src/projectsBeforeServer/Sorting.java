package projectsBeforeServer;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

class Sorting {
    int[] sortToIncrement (int[] inputArray){
        ArrayList<Integer> inputAsArrayList = new ArrayList<>();
        for (int anInputArray : inputArray) {
            inputAsArrayList.add(anInputArray);
        }
        int[] output = new int[inputArray.length];
        for (int i = 0; i < inputArray.length; i++) {
            int smallestInt = inputAsArrayList.get(0);
            for (Integer anInputAsArrayList : inputAsArrayList) {
                if (anInputAsArrayList < smallestInt) {
                    smallestInt = anInputAsArrayList;
                }
            }
            output[i] = smallestInt;
            //noinspection RedundantCollectionOperation
            inputAsArrayList.remove(inputAsArrayList.indexOf(smallestInt));
        }
        return output;
    }

    int[] sortToIncrementShortVersion (int[] inputArray){
        Arrays.sort(inputArray);
        return inputArray;
    }

    int[] bubbleSort (int [] inputArray){
        for (int i = 0; i < inputArray.length; i++) {
            for (int j = 0; j < inputArray.length-1; j++) {
                if(inputArray[j]>inputArray[j+1]){
                    int temp = inputArray[j];
                    inputArray[j] = inputArray[j+1];
                    inputArray[j+1] = temp;
                }
            }
        }
        return inputArray;
    }

    private static void sortByChangingArray(int[] inputArray){
        for (int i = 0; i < inputArray.length; i++) {
            for (int j = 0; j < inputArray.length-1; j++) {
                if(inputArray[j]>inputArray[j+1]){
                    int temp = inputArray[j];
                    inputArray[j] = inputArray[j+1];
                    inputArray[j+1] = temp;
                }
            }
        }
    }

    public static void main(String[] args) {
        int[] newArray = {1, 2, 3, 4, 5, 8, 33, 45, 86};
        System.out.println(newArray);
        sortByChangingArray(newArray);
        System.out.println(newArray);
    }

}
