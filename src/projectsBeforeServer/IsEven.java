package projectsBeforeServer;

public class IsEven {

    int mod(@SuppressWarnings("SameParameterValue") int dividend, int divisor){
        while(dividend > 0){
            dividend -= divisor;
        }
        if(dividend==0){
            return dividend;
        }else {
            return dividend+divisor;
        }
    }

    boolean isEven(int i){
        return i%2==0;
    }

    boolean isEvenWithoutMod(int i){
        String str = Integer.toString(i);
        return str.endsWith("0")||str.endsWith("2")||str.endsWith("4")||str.endsWith("6")||str.endsWith("8");
    }

    boolean isEvenWithBitwise(int i){
        return (i & 1) == 0;
    }

    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        int counter = 0;
        for (int i = 0; i < 1000000000; i++) {
            IsEven isEven = new IsEven();
            isEven.isEven(i);
            if (isEven.isEven(i)) {
                counter++;
            }
        }
        long end = System.currentTimeMillis();
        System.out.println(counter);
        System.out.println("isEven aeg: " + (end - start));

        long startWithoutMod = System.currentTimeMillis();
        counter = 0;
        for (int i = 0; i < 1000000000; i++) {
            IsEven isEvenWithoutMod = new IsEven();
            isEvenWithoutMod.isEvenWithoutMod(i);
            if (isEvenWithoutMod.isEvenWithoutMod(i)) {
                counter++;
            }
        }
        long endWithoutMod = System.currentTimeMillis();
        System.out.println(counter);
        System.out.println("isEvenWithoutMod aeg: " + (endWithoutMod - startWithoutMod));

        long startWithBitwise = System.currentTimeMillis();
        counter = 0;
        for (int i = 0; i < 1000000000; i++) {
            IsEven isEvenWithBitwise = new IsEven();
            isEvenWithBitwise.isEvenWithBitwise(i);
            if (isEvenWithBitwise.isEvenWithBitwise(i)) {
                counter++;
            }
        }
        long endWithBitwise = System.currentTimeMillis();
        System.out.println(counter);
        System.out.println("isEvenWithBitwise aeg: " + (endWithBitwise - startWithBitwise));
    }
}
