package projectsBeforeServer;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

class Stream {

    List<Integer> returnsDouble(List<Integer> input){
        return input.stream()
                .map(e -> e*2)
                .collect(Collectors.toList());
    }

    List<Integer> returnsDoubleIfEven(List<Integer> input){
        return input.stream()
                .filter(e -> e%2==0)
                .map(e -> e*2)
                .collect(Collectors.toList());
    }

    List<Integer> returnsDoubleIfEvenRemovesRepetitionsAndSortsToDecrease(List<Integer> input){
        return input.stream()
                .distinct()
                .filter(e -> e%2==0)
                .map(e -> e*2)
                .sorted(Comparator.reverseOrder())
                .collect(Collectors.toList());
    }

    boolean isAnySquareAPrime(List<Integer> input){
        return input.stream()
                .map(e -> e*e)
                .anyMatch(Stream::isPrime);
    }

    private static boolean isPrime(int number) {
        for (int i = 2; i <= number / 2; i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }

    boolean isDoubledNumberDivisibleBy4(List<Integer> input){
        return input.stream()
                .map(e -> e*2)
                .allMatch(e -> e%4==0);
    }
}
