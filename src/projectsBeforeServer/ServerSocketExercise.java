package projectsBeforeServer;

import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


public class ServerSocketExercise {

    @SuppressWarnings("SameParameterValue")
    private void start(int port) {
        try {
            ServerSocket serverSocket = new ServerSocket(port);
            System.out.println("Listening for connection on port " + port);

            //noinspection InfiniteLoopStatement
            while (true) {
                Socket clientSocket = serverSocket.accept();
                System.out.println("Connection opened to " + clientSocket.getRemoteSocketAddress());

                BufferedReader inReader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

                List<String> allInputLines = readAllInput(inReader);
                String reference = getReference(allInputLines).toLowerCase();
                URL url = getUrl(reference);

                OutputStream outputStream = clientSocket.getOutputStream();

                if (retrieveHTTPMethod(allInputLines).equals("GET")) {
                    if (!isReferenceAFile(url)) {
                        respondToClient(headerBuilderForNotFiles(url, retrieveHTTPMethod(allInputLines)), outputStream);
                    } else {
                        String fileName = url.getPath().substring(1);
                        if (doesFileExist(fileName)) {
                            byte[] fullInformationToSend = concatenatesByteArrays(headerBuilderForFiles(fileName), fileToByteArray(fileName));
                            respondToClient(fullInformationToSend, outputStream);
                        } else {
                            byte[] fullInformationToSend = concatenatesByteArrays(headerBuilderForFiles("404page.png"), fileToByteArray("404page.png"));
                            respondToClient(fullInformationToSend, outputStream);
                        }
                    }
                } else if (retrieveHTTPMethod(allInputLines).equals("HEAD")) {
                    if (!isReferenceAFile(url)) {
                        respondToClient(headerBuilderForNotFiles(url, retrieveHTTPMethod(allInputLines)), outputStream);
                    } else {
                        String fileName = url.getPath().substring(1);
                        if (doesFileExist(fileName)) {
                            respondToClient(headerBuilderForFiles(fileName), outputStream);
                        } else {
                            respondToClient(headerBuilderForFiles("404page.png"), outputStream);
                        }
                    }
                }
                stop(inReader, clientSocket);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private List<String> readAllInput(BufferedReader inReader) throws IOException {
        List<String> allInputLines = new ArrayList<>();
        String inputLine;
        while ((inputLine = inReader.readLine()) != null) {
            System.out.println(inputLine);
            allInputLines.add(inputLine);
            if (inputLine.length() == 0) {
                break;
            }
        }
        return allInputLines;
    }

    private String retrieveHTTPMethod(List<String> allInputLines) {
        String method = "";
        for (String oneLine : allInputLines) {
            if (oneLine.length() > 0 && (oneLine.substring(0, 3).equals("GET") || oneLine.substring(0, 4).equals("HEAD"))) {
                method = oneLine.substring(0, 4).trim();
            }
        }
        return method;
    }

    String getReference(List<String> allInputLines) {
        String reference = "";
        for (String oneLine : allInputLines) {
            if (oneLine.length() > 0 && (oneLine.substring(0, 3).equals("GET") || oneLine.substring(0, 4).equals("HEAD"))) {
                String[] inputLineAsArray = oneLine.split(" ");
                reference = inputLineAsArray[1].substring(1);
            }
        }
        return reference;
    }

    private URL getUrl(String reference) throws MalformedURLException {
        URL baseUrl = new URL("http://localhost:7777");
        return new URL(baseUrl, reference);
    }

    boolean isReferenceAFile(URL url) {
        if (url.getPath().equals("")) {
            return false;
        } else {
            String path = url.getPath();
            return path.contains(".");
        }
    }

    boolean doesFileExist(String path) {
        return new File(path).exists();
    }

    byte[] fileToByteArray(String file) throws IOException {
        return Files.readAllBytes(Paths.get(new File(file).getPath()));
    }

    byte[] headerBuilderForFiles(String reference) throws IOException {
        byte[] fileAsByteArray = fileToByteArray(reference);
        String contentType;
        if (findFileType(reference).equalsIgnoreCase("ico")) {
            contentType = "image/x-icon";
        } else {
            contentType = URLConnection.guessContentTypeFromName(reference);
        }
        String httpMessage;
        if (reference.equalsIgnoreCase("404page.png")) {
            httpMessage = "404 Not Found";
        } else {
            httpMessage = "200 OK";
        }
        String response = "HTTP/1.1 " + httpMessage + "\nContent-Type: " + contentType + "; charset=utf-8\nContent-Length: " + fileAsByteArray.length + "\n\n";
        return response.getBytes(StandardCharsets.UTF_8);
    }

    byte[] headerBuilderForNotFiles(URL url, String httpMethod) {
        String response = "HTTP/1.1 200 OK\n";
        String fullGreeting = getFullGreeting(url);
        String contentLength = "Content-Length: " + fullGreeting.getBytes().length;
        if(httpMethod.equals("GET")) {
            response += contentLength + "\n\n" + fullGreeting;
        } else {
            response += contentLength + "\n\n";
        }
        return response.getBytes(StandardCharsets.UTF_8);
    }

    private String getFullGreeting(URL url) {
        String fullGreeting;
        if (url.getPath().equals("")) {
            fullGreeting = "Hello world!";
        } else {
            String greetingFromPath = url.getPath().substring(1);
            if (url.getQuery() != null) {
                String queryString = url.getQuery();
                String[] separateQueriesInArray = queryString.split("&");
                HashMap<String, String> queries = new HashMap<>();
                for (String oneQuery : separateQueriesInArray) {
                    String[] query = oneQuery.split("=");
                    queries.put(query[0], query[1]);
                }
                String name = queries.get("name");
                fullGreeting = greetingFromPath + " " + name;
            } else {
                fullGreeting = greetingFromPath;
            }
        }
        return fullGreeting;
    }

    String findFileType(String reference) {
        String[] referenceParts = reference.split("\\.");
        return referenceParts[1];
    }

    byte[] concatenatesByteArrays(byte[] a, byte[] b) {
        int lenA = a.length;
        int lenB = b.length;
        byte[] completeArray = Arrays.copyOf(a, lenA + lenB);
        System.arraycopy(b, 0, completeArray, lenA, lenB);
        return completeArray;
    }

    void respondToClient(byte[] informationToSend, OutputStream outputStream) throws IOException {
        outputStream.write(informationToSend);
    }

    private void stop(BufferedReader inReader, Socket clientSocket) throws IOException {
        inReader.close();
        clientSocket.close();
    }

    public static void main(String[] args) {
        ServerSocketExercise server = new ServerSocketExercise();
        server.start(7777);
    }
}
