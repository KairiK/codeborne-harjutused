package projectsBeforeServer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class SelectionSortWithComparator {

    static <T> List<T> sort(List<T> inputList, Comparator<T> comparator){
        List<T> outputList = new ArrayList<>(inputList);
        int n = outputList.size();

        for (int i = 0; i < n-1; i++){
            int lowestValue = i;
            for (int j = i+1; j < n; j++)
                if (comparator.compare(outputList.get(j), outputList.get(lowestValue))<=0)
                    lowestValue = j;
            T temp = outputList.get(lowestValue);
            outputList.set(lowestValue, outputList.get(i));
            outputList.set(i, temp);
        }
        return outputList;
    }

    public static void main(String args[]) {
        PersonalCode kairi = new PersonalCode("49006135216");
        PersonalCode adrian = new PersonalCode("50407150000");
        PersonalCode debora = new PersonalCode("60610180000");
        PersonalCode urmas = new PersonalCode("36007061111");
        PersonalCode olderFemale = new PersonalCode("21904232222");
        PersonalCode olderMale = new PersonalCode("11904232222");
        List<PersonalCode> persons = new ArrayList<>(Arrays.asList(kairi, olderMale, olderFemale, adrian, urmas, debora));

        GenderCompareFemalesFirst gc = new GenderCompareFemalesFirst();
        SortByMonth sbm = new SortByMonth();
        FemaleFirstAndSortByMonth ff = new FemaleFirstAndSortByMonth();
        List<PersonalCode> result = SelectionSortWithComparator.sort(persons, ff);
        for(PersonalCode person : result){
            System.out.println(person.personalCode);
        }

        List<Integer> integers = new ArrayList<>(Arrays.asList(2, 4, 6, 3, 5, 7, 9, 0, 3));
        SmallerFirst sf = new SmallerFirst();
        System.out.println(SelectionSortWithComparator.sort(integers, sf));

        List<String> words = new ArrayList<>(Arrays.asList("abracadabra", "capsicum", "banana", "devour", "road", "pie"));
        ShorterFirst shf = new ShorterFirst();
        System.out.println(SelectionSortWithComparator.sort(words, shf));
    }
}

class SmallerFirst implements Comparator<Integer>{

    @Override
    public int compare(Integer o1, Integer o2) {
        return o1-o2;
    }
}

class ShorterFirst implements Comparator<String>{

    @Override
    public int compare(String o1, String o2) {
        return o1.length()-o2.length();
    }
}
