package projectsBeforeServer;

import java.util.ArrayList;
import java.util.List;

class Concat {

    List<String> concat(List<String> a, List<String> b){
        List<String> concatenatedAB = new ArrayList<>();
        for (String anA : a) {
            //noinspection UseBulkOperation
            concatenatedAB.add(anA);
        }
        for (String aB : b) {
            //noinspection UseBulkOperation
            concatenatedAB.add(aB);
        }
        return concatenatedAB;
    }

    List<String> concatWithAddAll(List<String> a, List<String> b){
        List<String> c = new ArrayList<>(a);
        c.addAll(b);
        return c;
    }
}
