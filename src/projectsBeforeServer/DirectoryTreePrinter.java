package projectsBeforeServer;

import java.io.File;
import java.util.Objects;

public class DirectoryTreePrinter {

    private static void iterateThroughDirectory(File file, String buffer) {
        File[] contents = file.listFiles();
        if (contents != null) {
            for (File singleFile : contents) {
                if (singleFile.isDirectory()) {
                    System.out.println(buffer + "  " + singleFile.getName());
                    iterateThroughDirectory(singleFile, buffer + "  ");
                } else {
                    System.out.println(buffer + "  " + singleFile.getName());
                }
            }
        } else {
            System.out.println("Not a valid directory");
        }
    }

    static void directoryTreePrinter(String pathname) {
        System.out.println(pathname);
        iterateThroughDirectory(new File(pathname), "");
    }

    public static void main(String[] args) {
        directoryTreePrinter("DirectoryTreeTestFolder");
        System.out.println("##########");
        directoryTreeStringPrinter("DirectoryTreeTestFolder");
    }
    //###################################################################

    private static void directoryTreeStringPrinter(String pathname) {
        String contentString = "";
        directoryTreeStringBuilder(new File(pathname), "", contentString);
        System.out.println(contentString);
    }



    private static String directoryTreeStringBuilder(File file, String buffer, String contentString) {
        File listFile[] = file.listFiles();
        if (listFile != null) {
            for (File aListFile : listFile) {
                if (aListFile.isDirectory()) {
                    contentString += "|\t\t";
                    return directoryTreeStringBuilder(aListFile, buffer + " ", contentString);
                } else {
                    return contentString + "+---" + aListFile.getName();

                }
            }
        }
        return "";
    }
        /*for (File singleFile : file.listFiles()) {
            if (singleFile.isDirectory()) {
                return contentString + buffer + "  " + singleFile.getName() + "\n";
            } else {
                return contentString + directoryTreeStringBuilder(singleFile, buffer + "  ", contentString);
            }
        }*/

}
