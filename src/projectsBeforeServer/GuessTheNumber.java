package projectsBeforeServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class GuessTheNumber {

    static int randomNumberGenerator(){
        return (int) (Math.random()*100+1);
    }

    static boolean assessAnswer(int answer, int guess){
        if(answer==guess){
            System.out.println("Correct number. You have won!");
            return true;
        }else if(answer>guess){
            System.out.println("Try a bigger number.");
            return false;
        }else{
            System.out.println("Try a smaller number.");
            return false;
        }
    }

    static boolean checksAnswerCompliance(String answer){
        try {
            Integer.parseInt(answer);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private String readsUserResponse() {
        //Enter data using BufferReader
        BufferedReader reader =
                new BufferedReader(new InputStreamReader(System.in));

        // Reading data using readLine
        String guess = "";
        try {
            guess = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return guess;
    }

    static boolean isGameOver(int answer, String guess) {
        if (checksAnswerCompliance(guess)) {
            return GuessTheNumber.assessAnswer(answer, Integer.parseInt(guess));
        } else if (guess.equalsIgnoreCase("quit")) {
            return true;
        } else {
            System.out.println("Not a number, try again");
        }
        return false;
    }

    private void game(){
        int answer = randomNumberGenerator();
        boolean gameOver = false;
        System.out.println("Welcome to the game. Guess the number.");
        while(!gameOver) {
            System.out.println("Your guess: ");
            String guess = readsUserResponse();
            gameOver = isGameOver(answer, guess);
        }
        System.out.println("Game ended.");
    }

    public static void main(String[] args) {
        GuessTheNumber game = new GuessTheNumber();
        game.game();
    }
}
