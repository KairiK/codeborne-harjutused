package projectsBeforeServer;

import java.util.Arrays;
import java.util.stream.Collectors;

class PigLatin {

    String pigLatinGenerator(String input){
        if(input.length()==0){
            return "";
        }
        String [] wordsAsArray = input.split(" ");
        StringBuilder outPutString = new StringBuilder();
        for (String word: wordsAsArray) {
            String PigLatinizedWord = word.substring(1)+word.substring(0, 1)+"ay";
            outPutString.append(PigLatinizedWord).append(" ");
        }
        return outPutString.toString().trim();
    }

   String pigLatinGeneratorWithStream(String input){
        if(input.equals("")) return "";
        return Arrays.stream(input.split(" "))
                .map(x->x.substring(1).concat(x.substring(0, 1)))
                .map(x->x.concat("ay"))
                .collect(Collectors.joining(" "));
    }
}
